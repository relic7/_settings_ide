##########
# Commands
{
    "_id": ObjectId("507f1f77bcf86cd799439011"),
    "cmd_id": 1 ,
    "command": "show version",
    "method": "inline"
},
{
    "_id": ObjectId("507f191e810c19729de860ea"),
    "cmd_id": 2,
    "command": "show ip interface <interface> <interface_state>",
    "method": "inline",
    "depends": [{
                "key": "interface",
                "pre_command": "show interfaces",
                ## bgp issue --> needs to discover bgp neighbor to be able to run the main bgp command
                "lambda": ""
               },
               {
                "key": "interface_state",
                "pre_command": "show interface state",
                "regex": ""
               }]
},
{
    "cmd_id": "3",
    "_id": ObjectId("507f1f77bcf86cd79943a1e2"),
    "command": "some other command",
    "method": "attach"
}

#######
# Events / Device_Class_Subclass
{
    "_id": 1,
    "event_guid": "B9DC8FF06EE34406BC7D38A8835B91CC",
    "device_class_subclass": "Cisco Systems|Nexus 7018",
    "commands": [
                    ObjectId("507f1f77bcf86cd799439011"),
                    ObjectId("507f191e810c19729de860ea")
                ],
    "wi": ObjectId("507f1f77bcf86cd799439011")
}

########
# Work_Instructions
{
    "_id":  ObjectId("507f1f77bcf86cd799439011"),
    "wi_id": 1,
    "wi": "<html><h1>This is a Work instruction</h1>These are the contents of the WI.</html>"
}
