# Copyright 2009-2015 Ram Rachum.
# This program is distributed under the MIT license.

from .read_write_lock import ReadWriteLock
