from flask import flash, redirect
from coral.config.settings import retry_once
from flask_wtf import Form
from wtforms.fields import StringField, PasswordField, TextAreaField, \
                             BooleanField, SubmitField, SelectField, FieldList, FormField
from wtforms.validators import DataRequired
from flask_wtf.widgets import ListWidget
from mongoengine.errors import NotUniqueError
from AppCollection import DB

Enrichment = DB.Enrichment


# Get Actions returning values to form
class EnrichmentActions(Form):
    actions_id = SelectField(u'actions')


def enrichment_actions(request):
    enrichment = Enrichment.query.get('id')
    form = EnrichmentActions(request.POST, obj=enrichment)
    form.actions.data = enrichment.actions
    return form.actions.data
    # form = EnrichmentActions(request.POST, obj=user)
    # form.action_id.choices = [(a.id, a.description) for a in  Enrichment.actions.query.order_by('id')]


# POST Actions saving form data to db
class EnrichmentActionsListField(FormField):
    widget = ListWidget()
    # enrichment = Enrichment()

    def __init__(self, label='', validators=None, **kwargs):
        self.data = Enrichment.objects.get(id=id).first().only('actions')

    def _value(self):
        if self.data:
            return u', '.join(self.data)
        else:
            return u''

    def process_formdata(self):
        """Process Enrichment Form returning Actions associated."""
        if self.data:
            actions = [x.strip() for x in self.data[0].split(',')]
        else:
            actions = []


class EnrichmentForm(Form):
    description = StringField(u'Description')
    event_policy_guid = StringField(u'Event Policy Guid')
    event_policy_name = StringField(u'Event Policy Name')
    # actions = SelectMultipleField(ListField(u'Actions Performed for Enrichment'))
    actions = FieldList(FormField(EnrichmentActionsListField('actions', [DataRequired()])))
    notes = FieldList(u'Special Params Enrichment Requires')
    supported_devices = SelectField(u'Applicable Devices Enrichment will run on')
    submit = SubmitField('Save')


def add_edit_enrichment(request, **kwargs):
    enrichment = Enrichment()
    if 'id' in request.args:
        try:
            id = request.args['id']
            enrichment = Enrichment.objects(id=id).first()
            if not enrichment:
                enrichment = Enrichment()
                raise Exception("Enrichment with _id=%s not found on the database." % request.args['_id'])
        except Exception as exc:
            flash(exc, "danger")
            pass

    if request.method == 'GET':
        form = EnrichmentForm()
        form.description.data = enrichment.description
        form.event_policy_guid.data = enrichment.event_policy_guid
        form.event_policy_name.data = enrichment.event_policy_name
        form.actions.data = enrichment.actions # enrichment_actions(request)
        form.notes.data = enrichment.notes
        form.supported_devices.data = enrichment.supported_devices
        form.extensions.data = enrichment.extensions
        return render_view(form=form, title='Add or Edit Enrichment')

    elif request.method == 'POST':
        try:
            enrichment_insert = {
                        'description': request.form.get('description'),
                        'event_policy_guid': request.form.get('event_policy_guid'),
                        'event_policy_name': request.form.get('event_policy_name'),
                        'actions': request.form.get('actions'),
                        'notes': request.form.get('notes', ''),
                        'supported_devices': request.form.get('supported_devices', ''),
                        'extensions': request.form.get('extensions', '')
                        }

            uri_root = 'enrichment'
            enrichment = Enrichment(**enrichment_insert)
            enrichment.ensure_index('id', unique=True)  # mongodb2.6+ .create_index({"id": 1})
            enrichment_obj = retry_once(enrichment.save)
            # enrichment_uri = "{}/{}/{}".format(home_url,
            #                                    uri_root,
            #                                    enrichment_obj.pk)  # <path:colname>/<path:id>
            flash("Successful Insert {}".format(enrichment_obj.description), 'success')
            return redirect('/coralapp/ted/add_edit_enrichment?id=%s&title=%s' % (enrichment.id, 'Enrichment' ))
        except NotUniqueError:
            flash("Error: [ Duplicate Key: {0} ] --> Cannot Ensure/Create Unique Index on Insert".format(request.form.get('enrichment_obj.description')), 'danger')
            return redirect('/coralapp/ted/add_edit_enrichment')
        except Exception as exc:
            flash("%s" % exc, 'danger')
            return redirect('/coralapp/ted/add_edit_enrichment')
