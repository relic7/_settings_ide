import mongoengine
from mongoengine import *
import datetime
import uuid
import json

# class ActionResult(Document):
#     meta = {
#         "indexes": ["action_id"]
#     }
#     action_id = ObjectIdField(required=True)
#     method = StringField(choices=["inline","attach"], default="inline")
#     result = DynamicField()
#     logfile = StringField(required=False)
#
# class ActionSpec(EmbeddedDocument):
#     """
#     An ActionSpec is a specification passed to an ActionRunner in order to produce a result.
#     """
#     spec = StringField()  # The argument spec. The action class interprets this value to produce a result.
#     filter = StringField(required=False) # optional regex to apply to result
#     capture_group = IntField(required=False) # Used with filter to set result to a capture group
#     context = StringField(required=False)  # If specified, it is the key extracted from context.
#
#
# class Action(Document):
#     description = StringField()
#     module = StringField()  # The name of the ActionRunner module
#     tokens = ListField(field=MapField(EmbeddedDocumentField(ActionSpec)))
#     options = DictField(default={})
#     method = StringField(choices=["inline","attach"], default="inline")
#     spec = EmbeddedDocumentField(ActionSpec, required=True)
#
# class Enrichment(Document):
#     meta = {
#         "indexes": ["event_guid"]
#     }
#     description = StringField()
#     event_guid = StringField()
#     event_name = StringField()
#     actions = ListField(ReferenceField(Action))

import mongoengine
from mongoengine import *
import datetime
import uuid
import json


MCHOICES = ["inline", "attach", "hidden"]
ACHOICES = ["Command", "Wi", "Snippet", "Http"]

def uniq_primary_uuid(model_cls):
    """ This generates a random string of 32 bits
        using currenttime float + uuid then randomly sampled
        for using alternate primary key thats guaranteed to be unique.
    :return: random.sample(random, 32)
    """
    import time, uuid, random
    x = str(time.time()).replace(".", "")
    y = uuid.uuid4().get_hex().__str__()
    random_id = "".join(random.sample((x+y), 32))
    try:
        while model_cls.objects.get(id=str(random_id)[:18]).first() is not None:
            random_id = "".join(random.sample((x+y), 32))
    except DoesNotExist:
        return random_id


class ActionResult(Document):
    meta = {
        "indexes": ["action_id"]
    }
    # id = StringField(primary_key=True, required=True, default=uniq_primary_uuid(ActionResult), unique=True)
    action_id = ObjectIdField(required=True)
    result = DynamicField()
    logfile = StringField(required=False)
    # timestamp=
    # ticket_id=


class Action_Base(Document):
    meta = {'allow_inheritance': True}

class Action(Action_Base):
    meta = {
        "indexes": ["id"]
    }
    id = StringField(primary_key=True, required=True, default=uniq_primary_uuid(Action_Base), unique=True)
    name = StringField()
    description = StringField()
    module = StringField(choices=ACHOICES) # The name of the ActionRunner module
    tokens = ListField(field=MapField(EmbeddedDocumentField("Action")))
    method = StringField(choices=MCHOICES, default=MCHOICES[0])
    spec = StringField(required=True)  # The argument spec. The action class interprets this value to produce a result.
    parser = StringField()
    run_async = BooleanField(default=False)
    # variants = ListField(DictField()) # to define overrides to existing fields. [ { "variant_id": "1", "method": "hidden" }, { "variant_id": "2", "method": "attach" } ]
    extensions = DictField()  # future-proofing



class Enrichment_Base(Document):
    meta = {'allow_inheritance': True}

class Enrichment(Enrichment_Base):
    meta = {
        "indexes": ["event_guid", "supported_devices"]
    }
    id = StringField(primary_key=True, required=True, default=uniq_primary_uuid(Enrichment_Base), unique=True)
    name = StringField()
    description = StringField()
    event_guid = StringField(required=True)
    event_policy_name = StringField()  ## to be removed only for initial import to match wiith api lookup to get guid
    event_name = StringField()  # Just for reference and alerting (to detect mismatches due to modified event policies). Not used for decision making at this point.
    supported_devices = DictField()  # Each key should be "mappable" to existing keys on Coral's Inventory or Device Class objects
    actions = ListField(ReferenceField(Action))
    extensions = DictField()  # future-proofing


class DB(Document):
    Action = Action
    Enrichment = Enrichment
    ActionResult = ActionResult
