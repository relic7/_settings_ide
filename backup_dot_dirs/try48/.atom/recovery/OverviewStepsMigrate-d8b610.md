[**policy**:IOS-LDP-Advanced]

_PolicyName = CMS: Cisco-IOS: LDP Neighbor Down Advanced_

**AlignedEvents** = [__"CISCO CMSCMO NW:ROUTING-LDP-5-ADJ_CHANGE Down"__]

**AlignedActions** = [
        "InitializeEnrichmentEngine",

```
            "IOS-LDP-Advanced",
```
<br<=>
        "CLIEnrichInline", "AttachNote", "LoadWorkInstructions", "StopEnrichmentEngine"
        ]

[action:**IOS-LDP-Advanced**]

_ActionName = CMS: Cisco-IOS: LDP Show Commands Advanced_

_Description = Cisco IOS MPLS LDP Show Commands Advanced._

**SnippetCodePath** = CiscoIOS/LDP/**stats\_show\_commands\_advanced.py**

<br>

#### A. Go through all the config .ini files
 -  Look for the **policy** blocks.

#### B. Go through each of the names in the AlignedEvents list and match them to a GUID (Richard's spreadsheet)

#### C. For each GUID, create one enrichment shell and
        - Look at the AlignedActions list, get the 2nd element and look for the "action block" with the same name
        - Get the SnippetCodePath file, parse it and add to the spec of the previously created Enrichment shell
        - Look for a WI file with the GUID for this enrichment shell and add it too.
        - Look at all the .j2 WI files, for each try to find an Enrichment Shell with the same GUID.

#### D. If it doesn't exist, create a new one with only the WI
<br>

---

### 0. Get the GUID for the event policy - Richard's LIST:
        - "CISCO CMSCMO NW:ROUTING-LDP-5-ADJ_CHANGE Down" = 2485y3t458345467534575785745667578
        - "Fake Event policy just for fun" = 9783y5ut845ug48hghj5re6hj5r90hjhy0h
<br>

### 1. Get the name of the aligned action (2nd element on the list):
        - IOS-LDP-Advanced **maps to the snippet code path** CiscoIOS/LDP/stats\_show\_commands\_advanced.py
<br>

### 2. Create a NEW TES Action:
> id: 876578347594657864547

> spec: parsed content of the file we got on step 1

> action_type: LegacySnippet
<br>

### 3. Save the YAML file with the newly created action.
<br>

### 4. Keep the _ActionID_ in memory somewhere
<br>

### 5. Create 2 enrichment shells
>  each of them will align to a different event policy guid but both align to the same ActionID - from step 4

    id: 87g8yhy8ghiuvjryhjtu
    event_policy_guid: 2485y3t458345467534575785745667578
    event_policy_name: CISCO CMSCMO NW:ROUTING-LDP-5-ADJ_CHANGE Down
    actions:
    - 876578347594657864547
    - 39057356809458679450e867ue4iupyoj5yuh  # This is the id of a NEW TES Action (action_type=Wi) based on the lookup for a Wi from this event_policy_guid

### 5.1 We have the Event Policy GUID. Let's see if there's a WI file for this event policy. If it exists:
    - Create TES Action with action_type=Wi (let's say we got action id 39057356809458679450e867ue4iupyoj5yuh)
    - Align the new Wi action to the "shell"


    id: j098ew9nr7f8439hjy5jr6jty
    event_policy_guid: 9783y5ut845ug48hghj5re6hj5r90hjhy0h
    event_policy_name: Fake Event policy just for fun
    actions:
    - 876578347594657864547
    - m9r7nmer89tjbrty0yjirh  # This is the id of a NEW TES Action (action_type=Wi) based on the lookup for a Wi from this event_policy_guid (different from the previous)
<br>

## 6. Save the enrichment shells to files and we're done. Next block... :
