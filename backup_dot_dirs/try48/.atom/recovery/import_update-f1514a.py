from flask import flash, redirect, request, jsonify
from werkzeug.utils import secure_filename
## For Dev only
#APP_BASE_DIR = '/data/apps/tes'
#APP_UPLOAD = '/coral-upload/tes'
#home_url = '/coralapps/tes'
###
from coral.config.settings import retry_once
from flask_wtf import Form
from wtforms.fields import StringField, FileField, SubmitField
from wtforms.validators import DataRequired
from os import path
import json, yaml, re
from AppModule import YamlProcessorUtilities
from AppCollection import DB
import APP_BASE_DIR
import APP_UPLOAD
import home_url

Action = DB.Action
Enrichment = DB.Enrichment

YamlActionProcessor = YamlProcessorUtilities.YamlActionProcessor
YamlEnrichmentProcessor = YamlProcessorUtilities.YamlEnrichmentProcessor


class UploadForm(Form):
    files = FileField(u'YAML File')
    name = StringField(u'YAML ID')
    submit = SubmitField('Save File')

    def validate_file(form, field):
        if field.data:
            field.data = re.sub(r'[^-]', '', field.data)


MCHOICES = ["inline", "attach", "hidden"]
ACHOICES = ["Command", "Wi", "Snippet", "Http"]


def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        f.save(secure_filename(f.filename))
        return json.dumps(f.read())



def simple_yaml_processor(yaml_file):
    stream = file(yaml_file, 'r')
    yaml_loaded = yaml.load(stream)
    return yaml_loaded

def simple_json_processor(json_file):
    return json.dumps(json_file.read())


def import_update(request, **kwargs):
    if request.method == 'GET':
        return render_view(title="Upload Yaml Docs for TES")
    elif request.method == 'POST':
        if request.files:
            if len(request.files) > 1:
                for f in request.files['files']:
                    parsed_yaml = simple_yaml_processor(f)

                flash("Uploaded and Stored: %s" % json.dumps(parsed_yaml), 'success')
                return render_view(title="Upload Yaml Docs for TES", count=len(request.files), example=json.dumps(parsed_yaml))

            else:
                print 'DEBUG ' + str(dir(request.files))
                f = request.files['files']
                parsed_yaml = simple_yaml_processor(f.read())
                if parsed_yaml:
                    flash("Uploaded and Stored: %s" % parsed_yaml, 'success')
                    return render_view(title="Upload Yaml Docs for TES", parsed_yaml=parsed_yaml)
