from flask import jsonify, redirect, flash
import requests

requests.packages.urllib3.disable_warnings()
import json
from coral.celerytasks import cmsapi
from coral.config import settings
from collections import OrderedDict
from datetime import datetime
from flask import flash, redirect
from coral.config.settings import retry_once
from bson import ObjectId
##
from coral.models import AppCollection
from coral.models import App as CoralApp
apps = CoralApp.objects().all()
tes_app_id = [ a.id for a in apps if a.name == 'TES' ]
import home_url
#tes= App.objects(id="5c20b27b7006080092b2825b").first()
#cols = [c for c in tes.collections.pop() ]
TESApp = CoralApp.objects(id=tes_app_id[0]).first()
collections = {comp.name: comp for comp in TESApp.collections}
#
ted2_app_id = [ str(a.id) for a in apps if a.name == 'TED2' ]
#
datastore_url = home_url.replace('coralapp/tes', 'apps/manage/datastore/' + str(tes_app_id[0]) )
#
#
from AppCollection import TEDDB
Enrichment = TEDDB.Enrichment
Action = TEDDB.Action

def get_enrichments(request, *args, **kwargs):
    if request.method == 'POST':
        global_search = request.values['search[value]']
        # which column to order by
        order_col = request.values['order[7][column]']

        # order direction
        order_col_dir = request.values['order[7][dir]']
        order_col_name = request.values['columns[%s][name]' % order_col]

        start = request.values['start']
        length = request.values['length']
        draw = request.values['draw']

        # individual column search
        col_keys = [key for key in request.values.keys() if
                    key.startswith('columns[') and key.endswith('][search][value]')]
        col_keys.sort()
        for index, col in enumerate(col_keys):
            if request.values[col]:
                search_col_name = request.values['columns[%s][name]' % index]


        logger.info("Fetching enrichments from internal collections/mongodb...")

        # if global_search != '':
        #     endpoint += "&filter.message.contains=%s" % global_search
        #
        # if order_col and order_col_name and order_col_dir:
        #     endpoint += "&order.%s=%s" % (order_col_name, order_col_dir)
        #
        # if start:
        #     endpoint += "&offset=%s" % start

        if int(length) == -1:
            enrichments = Enrichment.objects[int(start):]
        else:
            enrichments = Enrichment.objects[int(start):].limit(int(length))

        num_of_events_matched = enrichments.count()
        num_of_events_returned = enrichments.count(with_limit_and_skip=True)

        result = []  # initialize result as an empty list
        for e in enrichments:
            result.append({"event_policy_guid": e.event_policy_guid,
                           "event_policy_name": e.event_policy_name,
                           "actions_count": len(e.actions),
                           "description": e.description,
                           "clear_event_policy": e.clear_event_policy,
                           "notes": e.notes,
                           "supported_devices": e.supported_devices,
                           "modified": e.modified,
                           "extensions": e.extensions
                           })  # append a dict with the data for each row

        total_rows = num_of_events_matched
        filtered_rows = num_of_events_returned
        # result = [{"event_policy_guid": "123123", "actions_count": 3}, {"event_policy_guid": "123123", "actions_count": 3}, {"event_policy_guid": "123123", "actions_count": 3}]

        # build dictionary with values to return to datatables
        return jsonify({
            'draw': draw,
            'recordsTotal': total_rows,
            'recordsFiltered': filtered_rows,
            'data': result})


    else:
        return "Method not allowed", 405
