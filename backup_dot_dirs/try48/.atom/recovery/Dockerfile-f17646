#ARG REPO="containers.cisco.com/coral-team/coral-services"
ARG REPO="coral-services"
ARG TAG="latest-profiler"

FROM $REPO:$TAG

# Create all the required directories in container for jupyter
RUN pip install ipython==5.7.0 ipykernel==4.9.0; pip install jupyter==1.0.0 && \
    mkdir -p /root/.ipython/profile_default/startup && \
    mkdir -p /root/.ipython/profile_default/static/custom && \
    mkdir -p /root/.ipython/nbextensions && \
    mkdir -p /root/.jupyter/nbconfig/notebook.d && \
    mkdir -p "/root/.jupyter/lab/user-settings/@jupyterlab" && \
    mkdir -p /root/.jupyter/custom && \
    mkdir -p /root/.local/share/jupyter/nbextensions && \
    mkdir -p /root/.local/etc/jupyter/nbconfig && \
    mkdir -p /root/.local/etc/jupyter/jupyter_notebook_extensions.d && \
    mkdir -p /usr/local/etc/jupyter/jupyter_notebook_extensions.d && \
    mkdir -p /usr/local/etc/jupyter/nbconfig && \
    mkdir -p /data/ipynb-notebooks/skeletons && \
    mkdir -p /data/ipynb-notebooks/comet && \
    touch  /data/ipynb-notebooks/skeletons/cs1Untitled.ipynb && \
    mkdir -p /data/debugging_tools && \
    touch  /data/debugging_tools/flask_profiler.db && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /data/appdynamics/logs/dbagent && \
    mkdir -p /data/appdynamics/logs/pythonagent;

### LogIO Setup and Install
RUN apt-get update \
    && curl -sL https://deb.nodesource.com/setup_0.10 | bash - \
    && apt-get -y install nodejs \
    && adduser --shell /bin/false --gecos "" --disabled-password logio \
    && export USER=logio; export HOME=/home/logio;npm install -g log.io --user "logio" \
    && mkdir -p /coral-services/coral/logs/logio \
    && groupadd logs \
    && usermod -aG logs logio \
    && usermod -aG logs commands \
    && usermod -aG logs mongodb \
    && usermod -aG logs splunk \
    && chgrp -R logs /coral-services/coral/logs \
    && chgrp -R logs /var/log \
    && touch /coral-services/coral/logs/nginx/nginx.access_log \
    && rm -rf /var/lib/apt/lists/*;

# Install Java jre
RUN apt-get update && apt-get -y install default-jre;

ADD log.io/ /home/logio/.log.io/
ADD logio-server.supervisor.conf /etc/supervisor/conf.d/
ADD logio-harvester.supervisor.conf /etc/supervisor/conf.d/
ADD coral-services-dev.supervisor.conf /etc/supervisor/supervisord.conf
ADD dev_start_container.sh /coral-services/dev_start_container.sh
### Add config files for Jupyter and IPython plus Files for Coral Integration
ADD jupyter.supervisor.conf /etc/supervisor/conf.d/
ADD nginx.coral-services-dev.conf /etc/nginx/sites-enabled/nginx.coral-services.conf
ADD coral-templates/base.html /coral-services/coral/templates/base.html
ADD nbconfig/ipynb_default.py /root/.ipython/profile_default/startup/00-init.py
ADD nbconfig/root_.local/etc/jupyter/*  /root/.local/etc/jupyter/
COPY nbconfig/root_.local/share/jupyter/*.js  /root/.local/share/jupyter/
COPY nbconfig/root_.local/share/jupyter/nbextensions/*  /root/.local/share/jupyter/nbextensions/
COPY nbconfig/root_.jupyter/nbconfig/*.json  /root/.jupyter/
COPY nbconfig/root_.jupyter/nbconfig/notebook.d/*  /root/.jupyter/nbconfig/notebook.d/
COPY nbconfig/root_.jupyter/lab/user-settings/@jupyterlab/*  /root/.jupyter/lab/user-settings/@jupyterlab/
ADD nbconfig/jupyter_notebook_config.py /root/.jupyter/
##
ADD nbconfig/jupyter_nbconvert_config.json /usr/local/etc/jupyter/
ADD nbconfig/jupyter_notebook_config.json /usr/local/etc/jupyter/
#
ADD nbconfig/notebook.json /usr/local/etc/jupyter/nbconfig/
ADD nbconfig/tree.json /usr/local/etc/jupyter/nbconfig/
ADD nbconfig/edit.json /usr/local/etc/jupyter/nbconfig/
#
ADD nbconfig/custom/custom.js /root/.jupyter/custom/
ADD nbconfig/custom/custom.js /root/.ipython/profile_default/static/custom/
ADD nbconfig/custom/custom.css /root/.jupyter/custom/
ADD nbconfig/custom/custom.css /root/.ipython/profile_default/static/custom/
ADD nbconfig/nblab_node_extras/.npmrc  /root/
ADD nbconfig/nblab_node_extras/*.json  /root/
ADD nbconfig/nblab_node_extras/jupyterlab-variableInspector/  /root/

##--## Quick Fiox for Logio Failing
RUN cp -R /home/logio/.log.io /root/
##--##

# Monitoring Dashboard Config
# ADD flask_monitoringdashboard.cfg /coral-services/dev/flask_monitoringdashboard.cfg
### Python Requirements
ADD requirements.txt /coral-services/dev/requirements.txt

########################################################################
## Install and enable stable jupyter extensions in a single RUN cmd   ##
########################################################################
RUN apt-get update \
    && curl -sL https://deb.nodesource.com/setup_8.x | bash - \
    && apt-get install -y nodejs apt-utils gcc g++ make \
    && curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update && apt-get install yarn \
    && cd /root \
    && npm install -g \
    && rm -rf /var/lib/apt/lists/*;

RUN pip install --upgrade pip setuptools && \
    pip --no-cache-dir install -r /coral-services/dev/requirements.txt && \
    pip install --no-binary all -I --no-cache vpython && \
    jupyter contrib nbextension install --sys-prefix && \
    jupyter nbextension enable nbextensions_configurator/config_menu/main && \
    jupyter nbextension enable jupyter_cms --py --sys-prefix && \
    jupyter cms quick-setup --sys-prefix && \
    jupyter nbextension enable jupyter_dashboards --py --sys-prefix && \
    jupyter dashboards quick-setup --sys-prefix && \
    jupyter bundlerextension enable --sys-prefix --py dashboards_bundlers && \
    jupyter nbextension install /root/.local/share/jupyter/notebook-terminal-mode.js && \
    git clone https://github.com/minrk/nbextension-scratchpad  /usr/local/share/jupyter/nbextensions/nbextension-scratchpad && \
    jupyter nbextension install /usr/local/share/jupyter/nbextensions/nbextension-scratchpad && \
    jupyter nbextension enable nbextension-scratchpad/main && \
    git clone https://github.com/NII-cloud-operation/Jupyter-multi_outputs.git  /root/.ipython/nbextensions/Jupyter-multi_outputs && \
    jupyter nbextension install /root/.ipython/nbextensions/Jupyter-multi_outputs && \
    jupyter nbextension enable Jupyter-multi_outputs/main && \
    jupyter nbextension enable Jupyter-multi_outputs/lc_multi_outputs/nbextension/main && \
    jupyter nbextension enable codefolding/main && \
    jupyter nbextension enable code_font_size/code_font_size && \
    jupyter nbextension enable code_prettify/code_prettify && \
    jupyter nbextension enable code_prettify/2to3 && \
    jupyter nbextension enable code_prettify/main && \
    jupyter nbextension enable codemirror_mode_extensions/main && \
    jupyter nbextension enable collapsible_headings/main && \
    jupyter nbextension enable chrome-clipboard/main && \
    jupyter nbextension enable freeze/main && \
    jupyter nbextension enable execute_time/ExecuteTime && \
    jupyter nbextension enable help_panel/help_panel && \
    jupyter nbextension enable hide_input/main && \
    jupyter nbextension enable init_cell/main && \
    jupyter nbextension enable jupyter-js-widgets/extension && \
    jupyter nbextension enable keyboard_shortcut_editor/main && \
    jupyter nbextension enable limit_output/main && \
    jupyter nbextension enable livemdpreview/livemdpreview && \
    jupyter nbextension enable livemdpreview/main && \
    jupyter nbextension enable notify/notify && \
    jupyter nbextension enable python-markdown/main && \
    jupyter nbextension enable printview/main && \
    jupyter nbextension enable runtools/main && \
    jupyter nbextension enable search-replace/main && \
    jupyter nbextension enable select_keymap/main && \
    jupyter nbextension enable snippets_menu/main && \
    jupyter nbextension enable table_beautifier/main && \
    jupyter nbextension enable toc2/main && \
    jupyter nbextension enable toggle_all_line_numbers/main && \
    jupyter nbextension enable varInspector/main && \
    jupyter nbextension enable zenmode/main && \
    jupyter nbextension install --user --py jupyter_wysiwyg && \
    jupyter nbextension enable --py --sys-prefix ipytracer && \
    jupyter nbextension enable jupyter_wysiwyg --user --py && \
    jupyter nbextension enable theme_toggle;


ADD appdynamics/dbagent-4.3.7.3 /coral-services/dev/appdynamics/dbagent-4.3.7.3
ADD appdynamics/appd_dbagent.supervisor.conf /coral-services/dev/appdynamics/

## Install celery flower
RUN cd /tmp \
    && git clone https://github.com/mher/flower.git \
    && cd flower \
    && git checkout ef53832576e265e075f5dc14afd12005f8015aa1 \
    && /usr/local/bin/python setup.py install \
    && cd /coral-services \
    && rm -rf /tmp/flower \
    && cp /coral-services/dev/flower.supervisor.conf /etc/supervisor/conf.d/flower.supervisor.conf


# 5001: supervisorctl on own port since it can restart nginx
# 28777: log-server to receive logs from other node
EXPOSE 28777 5001

CMD /coral-services/dev_start_container.sh
