import mongoengine
from mongoengine import *
import datetime
import uuid
import json


MCHOICES = ["inline", "attach", "hidden"]
ACHOICES = ["Command", "Wi", "Snippet", "Http"]


class Action(Document):
    id = StringField(primary_key=True, default=uuid.uuid4().hex)
    description = StringField(required=True)
    action_type = StringField(required=True, choices=ACHOICES, default=ACHOICES[0])  # The name of the type of ActionRunner to use to run spec
    tokens = ListField(field=MapField(EmbeddedDocumentField("Action")))
    method = StringField(required=True, choices=MCHOICES, default=MCHOICES[0])
    spec = StringField(required=True)  # The argument spec. The action class interprets this value to produce a result.
    parser = StringField()
    run_async = BooleanField(required=True, default=False)
    extensions = DictField()  # future-proofing
    modified = DateTimeField(required=True, default=datetime.datetime.utcnow)   # modification or creation time for backend use


class Enrichment(Document):
    meta = {
        "indexes": ["event_policy_guid", "event_policy_name"]
    }
    id = StringField(primary_key=True, default=uuid.uuid4().hex)
    description = StringField(required=True)
    event_policy_guid = StringField(required=True, unique=True)
    event_policy_name = StringField(required=True)  # Just for reference and alerting (to detect mismatches due to modified event policies). Not used for decision making at this point.
    actions = ListField(ReferenceField(Action))
    notes = ListField(DictField())
    supported_devices = ListField()  # Ideally Each key should be "mappable" to existing keys on Coral's Inventory or Device Class objects, but list of Devices will suffice
    extensions = DictField()  # future-proofing
    modified = DateTimeField(required=True, default=datetime.datetime.utcnow)  # modification or creation time for backend use


class DB(Document):
    Action = Action
    Enrichment = Enrichment
