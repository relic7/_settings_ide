ARG REPO="containers.cisco.com/coral-team/coral-services"
#ARG REPO="coral-services"
ARG TAG="latest"

FROM $REPO:$TAG

# Create all the required directories in container for jupyter
RUN pip install ipython==5.7.0 ipykernel==4.9.0; pip install jupyter==1.0.0 && \
    mkdir -p /root/.ipython/profile_default/startup && \
    mkdir -p /root/.ipython/profile_default/static/custom && \
    mkdir -p /root/.ipython/nbextensions && \
    mkdir -p /root/.jupyter/nbconfig/notebook.d && \
    mkdir -p "/root/.jupyter/lab/user-settings/@jupyterlab" && \
    mkdir -p /root/.jupyter/custom && \
    mkdir -p /root/.local/share/jupyter/nbextensions && \
    mkdir -p /root/.local/etc/jupyter/nbconfig && \
    mkdir -p /root/.local/etc/jupyter/jupyter_notebook_extensions.d && \
    mkdir -p /root/.log.io && \
    mkdir -p /usr/local/etc/jupyter/jupyter_notebook_extensions.d && \
    mkdir -p /usr/local/etc/jupyter/nbconfig && \
    mkdir -p /data/ipynb-notebooks/skeletons && \
    mkdir -p /data/ipynb-notebooks/comet && \
    touch  /data/ipynb-notebooks/skeletons/cs1Untitled.ipynb && \
    mkdir -p /data/debugging_tools && \
    touch  /data/debugging_tools/flask_profiler.db && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /data/appdynamics/logs/dbagent && \
    mkdir -p /data/appdynamics/logs/pythonagent;

### LogIO Setup and Install
RUN apt-get update \
    && curl -sL https://deb.nodesource.com/setup_0.10 | bash - \
    && apt-get -y install nodejs \
    && adduser --shell /bin/false --gecos "" --disabled-password logio \
    && export USER=logio; export HOME=/home/logio; npm install -g log.io --user "logio" \
    && mkdir -p /coral-services/coral/logs/logio \
    && groupadd logs \
    && usermod -aG logs logio \
    && usermod -aG logs commands \
    && usermod -aG logs mongodb \
    && usermod -aG logs splunk \
    && chgrp -R logs /coral-services/coral/logs \
    && chgrp -R logs /var/log \
    && touch /coral-services/coral/logs/nginx/nginx.access_log \
    && touch /coral-services/coral/logs/logio/logio-harvester.log \
    && touch /coral-services/coral/logs/logio/logio-server.log \
    && rm -rf /var/lib/apt/lists/*;

# Install Java jre
RUN apt-get update && apt-get -y install default-jre;

ADD log.io/ /home/logio/.log.io/
ADD log.io/ /root/.log.io/
ADD logio-server.supervisor.conf /etc/supervisor/conf.d/
ADD logio-harvester.supervisor.conf /etc/supervisor/conf.d/
ADD coral-services-dev.supervisor.conf /etc/supervisor/supervisord.conf
ADD dev_start_container.sh /coral-services/dev_start_container.sh
### Add config files for Jupyter and IPython plus Files for Coral Integration
ADD jupyter.supervisor.conf /etc/supervisor/conf.d/
ADD nginx.coral-services-dev.conf /etc/nginx/sites-enabled/nginx.coral-services.conf
ADD coral-templates/base.html /coral-services/coral/templates/base.html
ADD nbconfig/ipynb_default.py /root/.ipython/profile_default/startup/00-init.py
ADD nbconfig/ipython_config.py /root/.ipython/profile_default/
ADD nbconfig/ipython_kernel_config.py /root/.ipython/profile_default/
ADD nbconfig/root_.local/etc/jupyter/*  /root/.local/etc/jupyter/
ADD nbconfig/root_.local/share/jupyter/*_*.js  /root/.local/share/jupyter/
ADD nbconfig/root_.local/share/jupyter/nbextensions/*  /root/.local/share/jupyter/nbextensions/
ADD nbconfig/root_.jupyter/nbconfig/*.json  /root/.jupyter/
ADD nbconfig/root_.jupyter/imongo_config.yml  /root/.jupyter/imongo_config.yml
ADD nbconfig/root_.jupyter/nbconfig/notebook.d/*  /root/.jupyter/nbconfig/notebook.d/
ADD nbconfig/root_.jupyter/lab/user-settings/@jupyterlab/*  /root/.jupyter/lab/user-settings/@jupyterlab/
ADD nbconfig/root_.local/share/jupyter/notebook-terminal-mode.js /root/.local/share/jupyter/
ADD nbconfig/jupyter_notebook_config.py /root/.jupyter/
##
# ADD nbconfig/jupyter_nbconvert_config_nbund.json /usr/local/etc/jupyter/
# ADD nbconfig/jupyter_notebook_config.json /usr/local/etc/jupyter/
#
# ADD nbconfig/notebook.json /usr/local/etc/jupyter/nbconfig/
# ADD nbconfig/tree.json /usr/local/etc/jupyter/nbconfig/
# ADD nbconfig/edit.json /usr/local/etc/jupyter/nbconfig/
#
ADD nbconfig/custom/custom.js /root/.jupyter/custom/
ADD nbconfig/custom/custom.js /root/.ipython/profile_default/static/custom/
ADD nbconfig/custom/custom.css /root/.jupyter/custom/
ADD nbconfig/custom/custom.css /root/.ipython/profile_default/static/custom/
ADD nbconfig/nblab_node_extras/.npmrc  /root/
ADD nbconfig/nblab_node_extras/*.json  /root/
ADD nbconfig/nblab_node_extras/jupyterlab-variableInspector/  /root/
##--## Quick Fix for Logio Failing
RUN cp -R /home/logio/.log.io /root/
##--##

# Monitoring Dashboard Config
# ADD flask_monitoringdashboard.cfg /coral-services/dev/flask_monitoringdashboard.cfg
### Python Requirements
ADD requirements.txt /coral-services/dev/requirements.txt

########################################################################
## Install and enable stable jupyter extensions in a single RUN cmd   ##
########################################################################
RUN apt-get update \
    && curl -sL https://deb.nodesource.com/setup_8.x | bash - \
    && apt-get install -y nodejs apt-utils gcc g++ make \
    && curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update && apt-get install yarn \
    && cd /root \
    && export USER=root; export HOME=/root; \
       pip install -r /coral-services/dev/requirements.txt \
    && export USER=root; export HOME=/root; npm install --save @jupyterlab/services --user "root" \
    && npm install -g --user "root" \
       react-paginate@^5.0.0 semver@^5.4.0 \
       jupyter-js-lab@^0.0.1 jupyter-js-widgets-labextension@^0.1.0 \
       jupyterlab-kernelspy@^0.4.0 jupyterlab-drawio@^0.4.0 jupyterlab_discovery@^5.1.3 \
       nbdime@^4.0.1 nbdime-jupyterlab@^0.6.0 \
       jupyterlab_iframe@^0.1.9 --user "root" \
    && npm install -g --user "root" \
    && rm -rf /var/lib/apt/lists/*;

# Jupyterlab extras*
RUN cd /root && npm install -g webpack && \
    # python -m pip install --upgrade pip setuptools && \
    # pip install --no-binary all -I --no-cache vpython && \
    pip install jupyter==1.0.0 six==1.12.0  jupyterlab-discovery==5.1.2 \
        jupyter_cms==0.7.0 ihtml==0.1.2 ipytracer==0.2.2.post1 \
        jupyter_dashboards==0.7.0 jupyter_dashboards_bundlers==0.9.1 \
        ipywidgets==7.4.2 widgetsnbextension==3.4.2 jupyter-js-widgets-nbextension==0.0.2.dev0 \
        widgetslabextension==0.1.0 jupyterlab-widgets==0.6.15 \
        pycodestyle_magic==0.2.5 pycodestyle==2.4.0 flake8==3.6.0 \
        nbresuse nbdime ansible_kernel logging_kernel bash_kernel\
        ipython==5.7.0 jupyterlab==0.33.12 jupyter-wysiwyg==0.1.9 sidecar==0.2.0  \
        jupyterlab_iframe==0.0.11 knowledgelab==0.0.1 sidepanel==0.3.0a0 && \
    pip install https://github.com/ipython-contrib/jupyter_contrib_nbextensions/tarball/master && \
    jupyter contrib nbextension install  --sys-prefix && \
    jupyter nbextension enable nbextensions_configurator/config_menu/main && \
    jupyter nbextension enable jupyter_cms --py --sys-prefix && \
    jupyter cms quick-setup --sys-prefix && \
    jupyter nbextension enable jupyter_dashboards --py --sys-prefix && \
    jupyter dashboards quick-setup --sys-prefix && \
    jupyter bundlerextension enable --sys-prefix --py dashboards_bundlers && \
    jupyter nbextension install /root/.local/share/jupyter/notebook-terminal-mode.js;
    # &&
    # pip install jupyterlab-git==0.5.0
    # jupyter labextension install @jupyterlab/git && \
    # jupyter serverextension enable --py jupyterlab_git && \
# pip install pythreejs qgrid lineup_widget extjs jupyterlab_commands \
 #   jupyterlab_templates jupyter_nbgallery nbnovnc && \
RUN jupyter nbextension install --sys-prefix --py jupyter_wysiwyg && \
    # jupyter nbextension install --py fileupload && \
    # jupyter nbextension install --py ipython_unittest --user && \
    # jupyter nbextension install --py jupygit --sys-prefix && \
    #jupyter nbextension install --py --symlink --sys-prefix pythreejs && \
    #jupyter nbextension install --py --symlink --sys-prefix qgrid && \
    #jupyter nbextension install --py jupyter_nbgallery && \
    # jupyter nbextension enable --py --sys-prefix bqplot && \
    #jupyter nbextension enable --py --sys-prefix extjs && \
    #jupyter nbextension enable --py --sys-prefix pythreejs && \
    #jupyter nbextension enable --py --sys-prefix qgrid && \
    jupyter nbextension enable --py --sys-prefix widgetsnbextension && \
    # jupyter nbextension enable --py fileupload && \
    # jupyter nbextension enable --py ipython_unittest --user && \
    # jupyter nbextension enable --py jupygit --sys-prefix && \
    #jupyter nbextension enable --py jupyter_nbgallery && \
    jupyter nbextension enable --py --sys-prefix ipytracer && \
    jupyter nbextension enable jupyter_wysiwyg --sys-prefix --py && \
    jupyter nbextension enable --py --sys-prefix widgetsnbextension && \
    #jupyter labextension install @mflevine/jupyterlab_html && \
    jupyter labextension install @jupyter-widgets/jupyterlab-manager && \
    #jupyter labextension install @jupyter-widgets/jupyterlab-sidecar && \
    jupyter labextension install jupyterlab-kernelspy && \
    jupyter labextension install jupyterlab-flake8 && \
    #jupyter labextension install @jpmorganchase/perspective-jupyterlab  && \
    jupyter labextension install @jupyterlab/celltags && \
    #jupyter labextension install @jupyterlab/git && \
    #jupyter labextension install @jupyterlab/plotly-extension@0.18.1 && \
    #jupyter labextension install @jupyterlab/statusbar && \
    #jupyter labextension install @krassowski/jupyterlab_go_to_definition && \
    #jupyter labextension install https://github.com/lckr/jupyterlab-variableInspector && \
    jupyter labextension install ipysheet  && \
    #jupyter labextension install jupyterlab_bokeh  && \
    #jupyter labextension install jupyterlab_commands  && \
    #jupyter labextension install jupyterlab_templates  && \
    #jupyter labextension install lineup_widget  && \
    #jupyter labextension install plotlywidget@0.6.0 && \
    # jupyter labextension install pylantern && \
    #jupyter labextension install qgrid && \
    #jupyter serverextension enable --py bookstore && \
    # jupyter serverextension enable --py jupygit --sys-prefix && \
    #jupyter serverextension enable --py jupyterlab_commands  && \
    #jupyter serverextension enable --py jupyterlab_git && \
    #jupyter serverextension enable --py jupyterlab_templates && \
    jupyter serverextension enable --py nbresuse && \
    # jupyter serverextension enable -–py lantern && \
    #jupyter serverextension enable --py jupyter_nbgallery && \
    jupyter serverextension enable --py --sys-prefix knowledgelab && \
    jupyter serverextension enable --py --sys-prefix jupyterlab_iframe && \
    #jupyter nbextension     install --py --sys-prefix nbnovnc && \
    #jupyter nbextension     enable  --py --sys-prefix nbnovnc && \
    #jupyter serverextension enable  --py --sys-prefix nbnovnc && \
    jupyter serverextension enable --sys-prefix jupyterlab_discovery;

# Jupyter Notebook basic extras
RUN git clone https://github.com/minrk/nbextension-scratchpad  /usr/local/share/jupyter/nbextensions/nbextension-scratchpad && \
    jupyter nbextension install /usr/local/share/jupyter/nbextensions/nbextension-scratchpad && \
    jupyter nbextension enable nbextension-scratchpad/main && \
    git clone https://github.com/NII-cloud-operation/Jupyter-multi_outputs.git  /root/.ipython/nbextensions/Jupyter-multi_outputs && \
    jupyter nbextension install /root/.ipython/nbextensions/Jupyter-multi_outputs && \
    jupyter nbextension enable /root/.ipython/nbextensions/Jupyter-multi_outputs/main && \
    jupyter nbextension enable /root/.ipython/nbextensions/Jupyter-multi_outputs/lc_multi_outputs/nbextension/main && \
    jupyter nbextension enable codefolding/main && \
    jupyter nbextension enable code_font_size/code_font_size && \
    jupyter nbextension enable code_prettify/code_prettify && \
    jupyter nbextension enable code_prettify/2to3 && \
    jupyter nbextension enable code_prettify/main && \
    jupyter nbextension enable codemirror_mode_extensions/main && \
    jupyter nbextension enable collapsible_headings/main && \
    jupyter nbextension enable chrome-clipboard/main && \
    jupyter nbextension enable freeze/main && \
    jupyter nbextension enable execute_time/ExecuteTime && \
    jupyter nbextension enable help_panel/help_panel && \
    jupyter nbextension enable hide_input/main && \
    jupyter nbextension enable init_cell/main && \
    jupyter nbextension enable jupyter-js-widgets/extension && \
    jupyter nbextension enable keyboard_shortcut_editor/main && \
    jupyter nbextension enable limit_output/main && \
    jupyter nbextension enable livemdpreview/livemdpreview && \
    jupyter nbextension enable livemdpreview/main && \
    jupyter nbextension enable notify/notify && \
    jupyter nbextension enable python-markdown/main && \
    jupyter nbextension enable printview/main && \
    jupyter nbextension enable runtools/main && \
    jupyter nbextension enable search-replace/main && \
    jupyter nbextension enable select_keymap/main && \
    jupyter nbextension enable snippets_menu/main && \
    jupyter nbextension enable table_beautifier/main && \
    jupyter nbextension enable toc2/main && \
    jupyter nbextension enable toggle_all_line_numbers/main && \
    jupyter nbextension enable varInspector/main && \
    jupyter nbextension enable zenmode/main && \
    jupyter nbextension enable theme_toggle;
    # cd /root && git clone https://github.com/lckr/jupyterlab-variableInspector && \
    # cd jupyterlab-variableInspector/ && npm install && npm run build && jupyter labextension install . && \

RUN pip install --upgrade pip && \
    pip install allthekernels ipykernel nameparser && \
    # ipykernel install ansible.install && \
    ipython kernel install allthekernels && \
    ipython kernel install ipykernel && \
    ipython kernel install bash_kernel.install && \
    ipython kernel install calysto_bash;


ADD appdynamics/dbagent-4.3.7.3 /coral-services/dev/appdynamics/dbagent-4.3.7.3
ADD appdynamics/appd_dbagent.supervisor.conf /coral-services/dev/appdynamics/

## Install celery flower
RUN cd /tmp \
    && pip install celery-flower==1.0.1 \
    && git clone https://github.com/mher/flower.git \
    && cd flower \
    && git checkout ef53832576e265e075f5dc14afd12005f8015aa1 \
    && /usr/local/bin/python setup.py install \
    && cd /coral-services \
    && rm -rf /tmp/flower \
    && cp /coral-services/dev/flower.supervisor.conf /etc/supervisor/conf.d/flower.supervisor.conf


# 5001: supervisorctl on own port since it can restart nginx
# 28777: log-server to receive logs from other node
EXPOSE 28777 5001

CMD /coral-services/dev_start_container.sh
