import CallbackHandler, AsyncFunction
import json

def install(request, **kwargs):
    logger.info("Hit '/install' endpoint")
    if request.method in ['GET']:
        args = request.args
        callback  = CallbackHandler.get_callback("InstallToEm7") 
        install_type = args['install_type']
        logger.info("Installation type: %s" % install_type)
        func = AsyncFunction(callback)
        ret = func(install_type=install_type)
        return ret
    else:
        return "Method not allowed", 405