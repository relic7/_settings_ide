from flask import flash, redirect
from AppModule import YamlProcessorUtilities
from YamlProcessorUtilities import extractall
from AppCollection import DB

Action = DB.Action
Enrichment = DB.Enrichment

YamlActionProcessor = YamlProcessorUtilities.YamlActionProcessor
YamlEnrichmentProcessor = YamlProcessorUtilities.YamlEnrichmentProcessor


def import_update(request, **kwargs):
    if request.method == 'GET':
        return render_view(title="Upload a tar, zip, or individual YAML Docs for TES")
    elif request.method == 'POST':
        if request.files:
            collection = request.form.get('collection')
            count = len(request.files.getlist("files[]"))
            if count > 1:
                filenames = []
                uploaded_files = request.files.getlist("files[]")
                ymlprocessor = ''
                for f in uploaded_files:
                    if collection == 'Action':
                        filenames.append(f.filename)
                        ymlprocessor = YamlActionProcessor(text_stream=f.read().decode('utf-8'), filename=f.filename)
                        try:
                            ymlprocessor.yaml_file_processor()
                            ymlprocessor.insert_data_to_mongo()
                            parsed_yaml = ymlprocessor.__dict__
                        except Exception as exc:
                            logger.critical(str(exc))
                            parsed_yaml = (str(exc))
                    elif collection == 'Enrichment':
                        filenames.append(f.filename)
                        ymlprocessor = YamlEnrichmentProcessor(text_stream=f.read())
                        try:
                            ymlprocessor.yaml_file_processor()
                            ymlprocessor.insert_data_to_mongo()
                            parsed_yaml = ymlprocessor.__dict__
                        except Exception as exc:
                            logger.critical(str(exc))
                            parsed_yaml = (str(exc))
                    else:
                        flash("The multiple files you Submitted do not contain a valid Action or Enrichment: %s" % request.files, 'danger')
                        return render_view(title="Try Again with at least 1 File")

                if count < 30:
                    flash("Uploaded and Stored: %s %s Files" % (str(filenames),
                                                                collection),
                          'success')
                    return render_view(title="Successful Import of YAML Docs for TES",
                                       count=count,
                                       filenames=filenames,
                                       example=str(parsed_yaml).split('\n'))
                else:
                    return render_view(title="Successful Import of YAML Docs for TES",
                                       count=count,
                                       example=str(parsed_yaml).split('\n'))
            elif count == 1:
                print '<---> DEBUG ' + str(request.files.to_dict().items())
                f = request.files.to_dict().popitem()
                filename = f[0]
                if filename.split('.')[-1] == 'yml':
                    ymlprocessor = YamlActionProcessor(text_stream=f[1].read().decode('utf-8'), filename=filename)
                    try:
                        ymlprocessor.yaml_file_processor()
                        ymlprocessor.insert_data_to_mongo()
                        parsed_yaml = ymlprocessor.__dict__
                    except Exception as exc:
                        logger.critical(str(exc))
                        parsed_yaml = (str(exc))
                    if parsed_yaml:
                        flash("One File %s has been Uploaded and Stored: %s" %
                              (collection, str(parsed_yaml)),
                              'success')
                        return render_view(title="That One File was successfully imported to TES",
                                           count=count,
                                           filenames=filename,
                                           parsed_yaml=parsed_yaml)
                elif filename.split('.')[-1] in ['zip', 'gz', 'tar']:
                    # Unpack Tarball or zip to list of files and process as multifile import
                    uploaded_files = extractall(f[1])
                    print(str(uploaded_files) + 'Line 85')
                    logger.info(str(uploaded_files))
                    # uploaded_files should now be all yml files contained in tarball
                    for f in uploaded_files:
                        print(str(f) + dir(f) + 'Line 89')
                        if collection == 'Action':
                            logger.critical(str(dir(f) + '--' + f))
                            filenames.append(f.filename)
                            ymlprocessor = YamlActionProcessor(text_stream=f.read().decode('utf-8'), filename=f.filename)
                            try:
                                ymlprocessor.yaml_file_processor()
                                ymlprocessor.insert_data_to_mongo()
                                parsed_yaml = ymlprocessor.__dict__
                            except Exception as exc:
                                logger.critical(str(exc) + '--' + 'tarballingAction')
                                parsed_yaml = (str(exc))
                        elif collection == 'Enrichment':
                            filenames.append(f.filename)
                            ymlprocessor = YamlEnrichmentProcessor(text_stream=f.read().decode('utf-8'))
                            try:
                                ymlprocessor.yaml_file_processor()
                                ymlprocessor.insert_data_to_mongo()
                                parsed_yaml = ymlprocessor.__dict__
                            except Exception as exc:
                                logger.critical(str(exc) + '--' + 'tarballingEnrich')
                                parsed_yaml = (str(exc))
                        else:
                            flash("The compressed file you Submitted Does not contain a valid yaml file of type Action or Enrichment: %s" % request.files.items(), 'danger')
                            return render_view(title="Try Again with at least 1 File with extension 'yml', 'zip' or 'tar.gz'")

                else:
                    flash("What you Submitted Does not contain a valid yaml file of type Action or Enrichment: %s" % dir(request.files['files[]']), 'danger')
                    return render_view(title="Try Again with at least 1 File with extension 'yml', 'zip' or 'tar.gz'")

        else:
            flash("You Didnt Submit any Files to Import: %s" % request.files, 'danger')
            return render_view(title="Try Again with at least 1 File")
