import mongoengine
from mongoengine import *
import datetime
import uuid
import json

MCHOICES = ["inline", "attach", "hidden"]
ACHOICES = ["Command", "Wi", "Snippet", "Http"]
#module = StringField(choices=ACHOICES)  # The name of the ActionRunner module

class ActionResult(Document):
    meta = {
        "indexes": ["action_id"]
    }
    action_id = ObjectIdField(required=True)
    result = DynamicField()
    logfile = StringField(required=False)
    # timestamp=
    # ticket_id=

    # timestamp=
    # ticket_id=


class Action(Document):
    meta = {
        "indexes": ["id"]
    }
    id = StringField(required=True, primary_key=True, default=lambda: str(uuid.uuid4()))
    name = StringField()
    description = StringField()
    module = StringField(choices=ACHOICES) # The name of the ActionRunner module
    tokens = ListField(field=MapField(EmbeddedDocumentField("Action")))
    method = StringField(choices=MCHOICES, default=MCHOICES[0])
    spec = StringField()  # The argument spec. The action class interprets this value to produce a result.
    parser = StringField()
    run_async = BooleanField(default=False)
    # variants = ListField(DictField()) # to define overrides to existing fields. [ { "variant_id": "1", "method": "hidden" }, { "variant_id": "2", "method": "attach" } ]
    extensions = DictField()  # future-proofing


class Enrichment(Document):
    meta = {
        "indexes": ["event_guid", "supported_devices"]
    }
    id = StringField(required=True, primary_key=True, default=lambda: str(uuid.uuid4()))
    name = StringField()
    description = StringField()
    event_guid = StringField(required=True)
    event_policy_name = StringField()  ## to be removed only for initial import to match wiith api lookup to get guid
    event_name = StringField()  # Just for reference and alerting (to detect mismatches due to modified event policies). Not used for decision making at this point.
    supported_devices = DictField()  # Each key should be "mappable" to existing keys on Coral's Inventory or Device Class objects
    actions = ListField(ReferenceField(Action))
    extensions = DictField()  # future-proofing


class DB(Document):
    Action = Action
    Enrichment = Enrichment
    ActionResult = ActionResult
