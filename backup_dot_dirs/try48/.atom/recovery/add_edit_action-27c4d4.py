from flask import flash, redirect
from coral.config.settings import retry_once
from flask_wtf import Form
from bson import ObjectId
from wtforms.fields import StringField, PasswordField, TextAreaField, BooleanField, SubmitField, SelectField
from wtforms.validators import DataRequired
from AppCollection import DB

Action = DB.Action


MCHOICES = [("inline", "inline"), ("attach", "attach"), ("hidden", "hidden")]
ACHOICES = [("Command", "Command"), ("Wi", "Wi"),
            ("Snippet", "Snippet"), ("Http", "Http"),
            ("Splunk", "Splunk"), ("LegacySnippet", "LegacySnippet")]


class ActionForm(Form):
    description = StringField(u'Description of Code')
    action_type = SelectField(u'Action Type', choices=ACHOICES)
    spec = TextAreaField(u'Code or Template')
    tokens = TextAreaField(u'Advanced or Additional Actions to be performed')
    method = SelectField(u'Method used for adding to ticket', choices=MCHOICES)
    parser = TextAreaField(u'Parser for Code Output')
    run_async = BooleanField(u'Code Runs In Order or Asynchronous')
    submit = SubmitField('Save')


def add_edit_action(request, **kwargs):
    action = Action()
    if 'id' in request.args:
        try:
            id = ObjectId(request.args['id'])
            action = Action.objects(id=id).first()
            if not action:
                action = Action()
                raise Exception("Action with id=%s not found on the database." % request.args['id'])
        except Exception as exc:
            flash(exc, "danger")
            pass

    if request.method == 'GET':
        form = ActionForm()
        form.description.data = action.description
        form.action_type.data = action.action_type
        form.spec.data = action.spec
        form.tokens.data = action.tokens
        form.method.data = action.method
        form.parser.data = action.parser
        form.run_async.data = action.run_async

        return render_view(title="Add/Edit Action", form=form)
    elif request.method == 'POST':
        try:

            action_insert = {
                              'description': request.form.get('description'),
                              'action_type': request.form.get('action_type'),
                              'spec': request.form.get('spec'),
                              'tokens': request.form.get('tokens', ""),
                              'method': request.form.get('method'),
                              'parser': request.form.get('parser', ""),
                              'run_async': request.form.get('run_async', False)
           }

            uri_root = 'action'
            action = Action(**action_insert)
            action.ensure_index('id', unique=True)  ## mongodb2.6+ .create_index({"id": 1})
            print dir(action)
            action_obj = action.save()
            action_uri = "{}/{}/{}".format(home_url, uri_root, action_obj.pk)  #  <path:colname>/<path:id>
            flash("Successful Insert {}".format(action_obj.description), 'success')
            return redirect(home_url)
        except NotUniqueError:
            flash("Error: [ Duplicate Key: {0} ] --> Cannot Ensure/Create Unique Index on Insert".format(request.form.get('action_obj.description')), 'danger')
            return redirect(home_url)
