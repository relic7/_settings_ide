# Create your app and enable debugging and app api debugging

from flask_debugtoolbar import DebugToolbarExtension
from flask_debug_api import DebugAPIExtension
import flask_monitoringdashboard as dashboard
# Register Flask-DebugToolbar and Flask-Debug-API
toolbar = DebugToolbarExtension()
app_toolbar = DebugAPIExtension()

# Register Monitoring Dashboard and Config
# Make sure that you first configure the dashboard, before binding it to your Flask application
# But Fail if not Dev Build. Dev build stores flask_monitoringdashboard.db file in /data/db/
try:
    dashboard_configured = dashboard.config.init_from(file='/coral-services/coral/dev/flask_monitoringdashboard.cfg')
except Exception as exc:
    print exc
