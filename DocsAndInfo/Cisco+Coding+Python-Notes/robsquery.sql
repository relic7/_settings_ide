SELECT children.tid AS parent_id, children.status_name AS parent_status, children.child_id, (
            SELECT t21.name
            FROM master_biz.ticket_states t21
            JOIN master_biz.ticketing t22 ON t21.tstate_id = t22.tstate_id
            WHERE t22.tid = children.child_id
        ) AS child_status, (

        SELECT STATUS FROM master_biz.ticketing
            WHERE tid = children.child_id
        ) AS child_status_code
        FROM (

    SELECT t1.tid, t2.name AS status_name, t1.master_tid, t3.type, t3.message, REGEXP_SUBSTR(
    t3.message, '[[:digit:]]+'
) AS child_id
    FROM master_biz.ticketing t1
    JOIN master_biz.ticket_states t2 ON t1.tstate_id = t2.tstate_id
    JOIN master_biz.ticket_logs t3 ON t1.tid = t3.tid
    WHERE t3.type LIKE 'Ticket A%'
    GROUP BY t1.tid, child_id
    HAVING t3.message LIKE '%Child [TID%'
    ORDER BY 3 ASC , 1 DESC
) AS children
    JOIN master_biz.ticket_states t12 ON children.status_name = t12.name
    WHERE children.tid = {em7values_ticket_id}
    LIMIT 0 , 30
