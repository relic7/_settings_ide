#!/bin/bash
#
# ensure running as root
if [ "$(id -u)" != "0" ]; then
  exec sudo "$0" "$@"
fi
#
DOCKER_IMAGE_NAME="${2:-latest}"
SHARED_VOL=${3:-DockerShare1}
#
# Run as daemon with -d
case "$1" in
    -d|--daemon)
        $0 < /dev/null &> /dev/null & disown
        exit 0
        ;;
    *)
        ;;
esac
#
docker container rm -f cs1-${HOSTNAME} ;
docker volume rm -f ${SHARED_VOL} ;
docker volume create ${SHARED_VOL} ;
echo "coral-services:${DOCKER_IMAGE_NAME}"
echo "Docker Run $(date)"
docker run -d --rm \
    --name cs1-${HOSTNAME} \
    --hostname cs1-${HOSTNAME} \
    -v ${SHARED_VOL}:/coral-services \
    -v /data01/commands:/data \
    -p 5000:5000 \
    -p 27017:27017 \
    -p 6000:6000 \
    -p 5671:5671 \
    -p 4369:4369 \
    -p 25672:25672 \
    -p 3000:15672 \
    -p 5001:5001 \
    -p 28777:28777 \
    -p 2222:22 \
    --add-host=splunk-head-deployment-server:10.201.149.149 \
    --add-host=splunk-momidx-01:10.201.149.149 \
    --add-host=splunk-momidx-02:10.201.149.149 \
    --env-file /opt/commands/env.list \
    coral-services:${DOCKER_IMAGE_NAME} &

sleep 4 ;
#
#

echo "Docker Permission Fix $(date)"
#
$(sudo chown -R root:docker /var/lib/docker/volumes /var/lib/docker/volumes/${SHARED_VOL} /var/lib/docker/volumes/${SHARED_VOL}/_data  ; sudo chmod 775 /var/lib/docker/ /var/lib/docker/volumes/ ; sudo chmod -R 775 /var/lib/docker/volumes/${SHARED_VOL} /var/lib/docker/volumes/${SHARED_VOL}/_data;) & 
#
## Move the .idea settings to the container volume
echo "Pycharm .idea cp $(date)"
sleep 4 ;
## Sync diff from repo dir to DockerShare1
/usr/bin/rsync --compress --update --exclude *.git/ --verbose /home/jbragato/virtualenvs/coral-services/ /var/lib/docker/volumes/DockerShare1/_data ;
# $(sudo cp -R /home/jbragato/virtualenvs/coral-services/.idea /var/lib/docker/volumes/${SHARED_VOL}/_data/  ; sudo chown -R root:docker /var/lib/docker/volumes/${SHARED_VOL}/_data/.idea/ ; sudo chmod -R 775 /var/lib/docker/volumes/${SHARED_VOL}/_data/.idea;) &

echo -e ${USER} ;
echo "DONE $(date)" ;
echo "Container cs1-${HOSTNAME} launched";
echo -e "Container Live Files in:\n\t /var/lib/docker/volumes/${SHARED_VOL}"

echo "Starting Coral Container SSH Service"
sleep 4
~/virtualenvs/coral-services/dev/enable_ssh.sh
echo "DONE..."

