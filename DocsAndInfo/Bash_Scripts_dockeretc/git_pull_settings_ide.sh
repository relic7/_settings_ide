#!/bin/bash

# . ~/.bash_profile

PWD=${1:-~/settings_ide_repo}

echo `date` >> ~/.${PWD}.log
cd ${PWD}
LOGFILE=~/.$(basename {PWD}).log
touch ${LOGFILE}
git pull origin master > ${LOGFILE}
git push origin master > ${LOGFILE}
