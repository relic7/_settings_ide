#!/bin/bash


export MSSQL_CONTAINER=mssql-dev

# docker network disconnect isolated_nw cs1-devubux
# sleep 1
# docker network connect --link "$MSSQL_CONTAINER":"$MSSQL_CONTAINER"-net --link "$JUPY_CONTAINER":"$JUPY_CONTAINER"-net isolated_nw cs1-devubux

docker run --rm -d \
    --privileged \
    --name "$MSSQL_CONTAINER" \
    --hostname "$MSSQL_CONTAINER" \
    -e 'ACCEPT_EULA=Y' \
    -e 'SA_PASSWORD=yourStrong123Password' \
    -p 1433:1433 relic7/mssql-msft-dev:latest

# sleep 5

# docker run -d --rm --name "$JUPY_CONTAINER" --hostname "$JUPY_CONTAINER" --link "$MSSQL_CONTAINER":"$MSSQL_CONTAINER"-net --link cs1-"$(hostname)":cs1-devubux  --network isolated_nw --ip=172.25.0.4 --volumes-from cs1-devubux -p 8888:8888 "$JUPY_CONTAINER"


# sleep 2

# docker network connect --link "$MSSQL_CONTAINER":"$MSSQL_CONTAINER"-net isolated_nw "$JUPY_CONTAINER"
# docker network connect --link "$JUPY_CONTAINER":"$JUPY_CONTAINER"-net isolated_nw "$MSSQL_CONTAINER"
