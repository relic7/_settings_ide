#!/bin/bash
set -x
export R1_VIRTUAL_IP=10.10.10.1;
export R1_DOCKER_IP=172.17.0.5;
export R2_VIRTUAL_IP=10.10.10.2;
export R2_DOCKER_IP=172.17.0.6;
# Assumes the default docker0 network of 172.17.0.0/16
# R1:
iptables -t nat -A PREROUTING -p tcp -s 172.17.0.0/16 -d $R1_VIRTUAL_IP/32 --dport 22 -j DNAT --to $R1_DOCKER_IP:22;
iptables -t nat -A PREROUTING -p tcp -s 172.17.0.0/16 -d $R2_VIRTUAL_IP/32 --dport 22 -j DNAT --to $R2_DOCKER_IP:22;
iptables -t nat -A PREROUTING -p udp -s 172.17.0.0/16 -d $R1_VIRTUAL_IP/32 --dport 161 -j DNAT --to $R1_DOCKER_IP:161;
iptables -t nat -A PREROUTING -p udp -s 172.17.0.0/16 -d $R2_VIRTUAL_IP/32 --dport 161 -j DNAT --to $R2_DOCKER_IP:161;
