#!/bin/bash

docker exec -it $(docker ps --filter ancestor=coral-services -q | head -1) python -c 'from coral.config import settings;db = settings.pymongo_client_factory()["coral"];print "backend: ", db.cli_taskmeta.remove({});print "subtasks: ", db.sub_task.remove({});print "tasks: ", db.tasks.remove({});'

