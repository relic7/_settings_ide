
#!/bin/bash

export JUPY_CONTAINER=jupyter-dev
export DB_CONTAINER=mysql-dev

# docker network disconnect isolated_nw cs1-devubux
sleep 1
# docker network connect --link "$DB_CONTAINER":"$DB_CONTAINER"-net --link "$JUPY_CONTAINER":"$JUPY_CONTAINER"-net isolated_nw cs1-devubux

# docker run --rm -d --privileged --name "$DB_CONTAINER" --hostname "$DB_CONTAINER" -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourStrong123Password' --volumes-from cs1-devubux -p 1433:1433 datagrip/mssql-server-linux
docker run --rm -d --privileged \
    --name "$DB_CONTAINER" \
    --hostname "$DB_CONTAINER" \
    -p 3306:3306 \
    -e PUID=1000 \
    -e PGID=1000 \
    -e MYSQL_ROOT_PASSWORD=coral \
    -v /data01/mysqldb:/config relic7/"$DB_CONTAINER"-data

# docker run -d --rm --name "$JUPY_CONTAINER" --hostname "$JUPY_CONTAINER" --link cs1-devubux --volumes-from cs1-devubux --link "$DB_CONTAINER" -p 8888:8888 "$JUPY_CONTAINER"


sleep 2

# docker network connect --link "$DB_CONTAINER":"$DB_CONTAINER"-net isolated_nw "$JUPY_CONTAINER"
# docker network connect --link "$JUPY_CONTAINER":"$JUPY_CONTAINER"-net isolated_nw "$DB_CONTAINER"




# docker create --name=mysql-dev -p 3306:3306 -e PUID=1000 -e PGID=1000 -e MYSQL_ROOT_PASSWORD=coral -v /data01/mysqldb:/config linuxserver/mysql

