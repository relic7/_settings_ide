#############################################
####  Bash Non-Login Interactive Shells #####
####  nb. If Shell is a login shell     #####
####  this is sourced from  ~/.bash_profile #
####  which will source this file as well  ##
#############################################
#
###################################
####  -- BASH_SHELL_OPTIONS -- ####
###################################
# Enable options:
shopt -s cdspell
shopt -s cdable_vars
shopt -s checkhash
shopt -s checkwinsize
shopt -s sourcepath
shopt -s no_empty_cmd_completion
shopt -s cmdhist
shopt -s histappend histreedit histverify
shopt -s extglob # Necessary for programmable completion.
#
# Disable options:
shopt -u mailwarn
unset MAILCHECK  # Don't want my shell to warn me of incoming mail.
######
#
#######################################################
### Noninteractive and Interactive Required Env Vars ##
#######################################################
export JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk1.8.0_172.jdk/Contents/Home/"
##  /System/Library/Frameworks/JavaVM.framework/Versions/Current/
export FISHEYE_INST="/Users/jbragato/virtualenvs/VIRTUAL_CORALENV_PYTHON_CRUCIBLE/crucible-instance"
######
###############################
####### Prompts and other  ####
####### visual Shell Options  #
###############################
##### COLORS COLORS colors ####
###############################
if
  ls --color > /dev/null 2>&1; then
## GNU ls
  colorflag="--color"
else
## OS X ls
  colorflag="-G"
fi
##################################
# Always use color output for `ls`
# Changes Default Behavior of ls
alias ls="ls ${colorflag}"
export LS_COLORS='no=00:fi=00:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.avi=01;35:*.fli=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.ogg=01;35:*.mp3=01;35:*.wav=01;35:'
##################################
### THE Prompts and Editors to use Non and Interactive
#. ~/.bash_prompt
export PS1="\[\033[03;35m\]@\H:\[\033[01;34m\]\$(__git_ps1 '(%s)')\[\033[00;31m\] \W/\[\033[00;34m\]-\$\[\033[00m\] "
## Editor to use by default
export EDITOR=/usr/bin/nano
export TERM=xterm-new
###################################
###########
## misc. ##
###########
#
##################################################
####        Additional MAN Pages                ##
##################################################
#
MANPATH="/usr/local/opt/gnu-sed/libexec/gnuman:${MANPATH}"
#
##################################################
## More Colors.... colors.. colors
##### Prompt Colors and such #####
## olive and blue with FQDN and PWD
#PS1='\[\033[02;32m\]\u@\H:\[\033[02;34m\]\w\$\[\033[00m\] '
## Standard variations
#PS1='\[\033[02;32m\]\u@\h:\[\033[00;33m\]\W/\[\033[00;36m\]-$\[\033[00m\] '
## with git
####
# export PS1="\[\033[03;35m\]@\H:\[\033[01;34m\]\$(__git_ps1 '(%s)')\[\033[00;31m\] \W/\[\033[00;34m\]-\$\[\033[00m\] "
####
# PS1='\[\033[02;32m\]\u@\H:\[\033[02;34m\]\W\$\[\033[00m\] '
## bg clrs too Ugly
# PS1='\[\033[02;32m\]\u@\H:\[\033[02;34m\]\w\$\[\033[44m\]\[\033[1;31m\] '
####################################################

############################################################
##### Source the Aliases and Functions Located Separately
##### An organizational based decision to split from .bashrc
#############################################################
# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2- | tr ' ' '\n')" scp sftp ssh;
# Load the aliases and functions
if [ -r ~/.bash_aliases ]; then . ~/.bash_aliases; fi
if [ -r ~/.bash_functions ]; then . ~/.bash_functions; fi
# Load the shell dotfiles including .functions and .extra
# * ~/.path can be used to extend `$PATH`.
# * ~/.extra can be used for other settings you don’t want to commit.
#for file in ~/.{path,bash_prompt,exports,bash_aliases,bash_functions,extra}; do
for file in ~/.{path,exports,bash_aliases,bash_functions,extra}; do
    [ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;
