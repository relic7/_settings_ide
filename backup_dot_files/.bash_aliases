
########################################################
##### ----- BASH_ALIASES ONLY ----- ####################
########################################################

########################################################
#######   Overide common bash commands behavior     ####
#######   using their options                       ####
#######   and setting them as aliases of themselves ####
########################################################
alias dirs='dirs -G'
alias du='/usr/bin/du'
###
alias functions="declare -f | grep '^[a-z].* ()' | sed 's/{$//'"
alias paths='echo -e ${PATH//:/\\n}'
alias find_bad_chars="find . -name '*.sls'  -exec  grep --color='auto' -P -n '[^\x00-\x7F]' \{} \;"
#####
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
# find console port for attaching by mac CMD line with screen (e.g screen /dev/tty.usbmodem1a21 9600)
alias console_port="ls -ltr /dev/*usb*"
####
########################################################
############### END OVERIDES OF COMMON CMDS ############
########################################################

####################################
#----------------------------------#
##  The Most Used Aliases of All  ##
#----------------------------------#
####################################
## Reload this File into Environment
alias RLoadBashProfile='. /Users/jbragato/.bash_profile'
alias sublime='sublime_text'
alias subl='sublime_text'
###
### Top alias Used Case Insensitive History Grep ###
alias h='history | grep -i'
# alias h='history | grep -e '
# history shortcut with grep Case Sensitive
alias hist='history | grep'
####################################
#----------------------------------#
####################################

#########################################
######## Coral Specific Aliases #########
#########################################
alias cdc='cd ~/virtualenvs/clean-coral-services-jupyterkernel'
alias cdC='cd ~/virtualenvs/coral-services'
alias cdI='cd /Users/jbragato/virtualenvs/coral-iol-docker'
alias cdCC='cd /var/lib/docker/volumes/DockerShare1/_data'
alias cdD="docker exec -it cs1-${HOSTNAME} bash"
alias cdPP='cd ~/virtualenvs/diagnostic-power-packs'
alias cd2='cd ~/virtualenvs/APP2DIR'
alias cdA='cd ~/virtualenvs/Automated_Deployment/CORAL_DEPLOYMENT/ANSIBLE_ROOT && . bin/activate && cd ansible-playbook-coral-core && echo "You Are Now in the ANSIBLE_ROOT VirtualEnvironment"'
alias cdH='cd /Users/jbragato/virtualenvs/coral-holodeck/holodeck'
alias cdtools='cd /Users/jbragato/virtualenvs/tooling_integration_external'
alias cdTES='cd /Users/jbragato/virtualenvs/TES_APP_ROOT'
alias cdE='cd /Users/jbragato/virtualenvs/TE-Enrichments-Dev'
##########################################
#
############################
###### Docker Aliases ######
############################
###  Docker Info aliases ###
alias Dps='docker ps | ccze -A'
alias Dimages='docker images | ccze -A'
alias Dimages='docker stats | ccze -A'
## Docker Maintainence and Garbage Collection ##
alias DPrune='docker image prune'
alias DcontainerPrune='docker container prune'
alias DvolumePrune='docker volume prune'
## Specific Use Case Aliases for Docker ##
alias DremoveAllCoralpyc="docker exec cs1-${HOSTNAME} find . -iname \*.pyc -exec rm {} \;"
alias DDeleteallTasksEtc='/Users/jbragato/bin/coral_docker_delete_taskdata.sh'
alias DDeletecontainersDev='docker rm -f cs1-devubux jupyter-dev mysql-dev'
alias DockerELK_VMOPTS='docker run -p 5601:5601 -p 9200:9200 -p 5044:5044 -it -e MAX_MAP_COUNT=262144 -e ES_HEAP_SIZE="2g" -e LS_HEAP_SIZE="1g" -e MAX_OPEN_FILES=65536 -e CLUSTER_NAME="elasticClusterLogFile"--name elk sebp/elk'
#
#####################
## Building Docker ##
#####################
# builds docker but requires tag as arg1
alias DBuild_addtag='docker build --no-cache ./ --tag'
alias DXBuild_addtag='docker build ./ --tag'

#
### Chmod the DOCKER Volumes from docker then run -- same as above with more permission fixes, and no 8888
# ie. docker volume create DockerShare1
# SOMEWHAT OBSOLETE Alais to run just use the scriprt in ~/bin #
##################################
alias runVolDockerShare='sudo chmod ugo+r /var/lib/docker /var/lib/docker/volumes && \
                         sudo chown -R root:docker /var/lib/docker/volumes/DockerShare1 /var/lib/docker/volumes/DockerShare1/_data && \
                         sudo chmod -R ug+rw /var/lib/docker/volumes/DockerShare1 /var/lib/docker/volumes/DockerShare1/_data && \
                         docker run --rm --name cs1-${HOSTNAME} --hostname cs1-${HOSTNAME} -v DockerShare1:/coral-services \
                           --add-host=splunk-head-deployment-server:10.201.149.149 --add-host=splunk-momidx-01:10.201.149.149 \
                           --add-host=splunk-momidx-02:10.201.149.149 \
                           -v /data01/commands:/data -p 5000:5000 -p 27017:27017 -p 6000:6000 -p 5671:5671 -p 4369:4369 -p 25672:25672 -p 3000:15672 \
                           --env-file /opt/commands/env.list coral-services'
#
## Same Obsolesence #############
alias dockerCopyPycharm2Container='sudo cp -R /Users/jbragato/virtualenvs/coral-services/.idea /var/lib/docker/volumes/DockerShare1/_data/ && \
                                   sudo chown -R root:staff /var/lib/docker/volumes/DockerShare1/_data/.idea && \
                                   sudo chmod -R 755 /var/lib/docker/volumes/DockerShare1/_data/.idea'
#
##################################
###  End Obsolete Aliases ########
##################################

##################################
## Monitoring Docker Containers ##
##################################
## cs1 suprvis status in container
alias DexecSprvisor="docker exec cs1-${HOSTNAME} supervisorctl status | ccze -A"
alias DBGCMD='tail -f coral/logs/flask/*.log coral/logs/uwsgi/uwsgi.log coral/logs/celery/*.log coral/logs/cmsapi.log coral/logs/mongodb/mongodb.log | ccze -A'
alias CoralTail='docker exec cs1-devubux tail -f coral/logs/flask/audit.log coral/logs/flask/flask.log coral/logs/uwsgi/uwsgi.log coral/logs/mongodb/mongodb.log | ccze -A'
alias CoralOk='curl -RLk https://127.0.0.1:5000'
#
#### Docker Info Aliases #####
## Ip and Port of Running Docker Containers
# alias DIipPorts="'"docker inspect --format "{{ .Name }} # {{ .NetworkSettings.IPAddress }} # {{ .NetworkSettings.Ports }}" $(docker ps -q) | tr -s '#' '\t'"'"
# Docker Volumes by Container
#alias DIvolsByContainer="'"docker ps -a --format '{{ .ID }}' | xargs -I {} docker inspect -f '{{ .Name }}{{ printf "\n" }}{{ range .Mounts }}{{ printf "\n\t" }}{{ .Type }} {{ if eq .Type "bind" }}{{ .Source }}{{ end }}{{ .Name }} => {{ .Destination }}{{ end }}{{ printf "\n" }}' {}"'"


##############################
# ****** End Docker ******** #
##############################

##############################################
##### Virtualenv and other Python Aliases ####
##############################################
### Activate Virtualenv from its basedir #####
alias ActivateVirtEnv='. bin/activate'.  #####
##############################################

#################
## Git Aliases ##
#################
alias GGetPull='git pull'
alias GCommit='git commit -a'
alias GFetch='git fetch'
alias GStatus='git status | ccze -A'
alias GDevJupyterChkout='git checkout dev-jupyter'
alias GMasterChkout='git checkout master'
##
##################
##### TicketEnrichments and TED
alias ted='python ~/virtualenvs/ted_cli/ted'
#####
##########################
# Sysadmin Aliased Tools #
##########################
##-- Memory and CPU Info --##
## Pass options to free ##
alias meminfo='free -m -l -t | ccze -A'
## Get top process eating memory
alias psmem='ps auxf | sort -nr -k 4 | ccze -A'
# alias psmem10='ps auxf | sort -nr -k 4 | head -10 | ccze -A'
# not sure what this next one revels
alias psg='ps aux | grep -v grep | grep -i -e VSZ -e | ccze -A'
## Get top process eating cpu ##
# alias pscpu='ps auxf | sort -nr -k 3 | ccze -A'
# alias pscpu10='ps auxf | sort -nr -k 3 | head -10 | ccze -A'
##
## Get server cpu info ##
### alias cpuinfo='lscpu'
## older system use /proc/cpuinfo ##
alias cpuinfo='less /proc/cpuinfo | ccze -A' ##
##
## Get GPU ram on desktop / laptop ##
alias gpumeminfo='grep -i --color memory /var/log/Xorg.0.log'
##
######################
## Disk Space Usage ##
alias df='df -H | ccze -A'
alias du='du -ch | ccze -A'
######################
#
########################
# Network Info Aliases #
########################
alias ports='netstat -p tcp | ccze -A'
alias ports-open='netstat -tulanp | ccze -A'
alias netmem='netstat -m | | ccze -A'
## Curl HTTP Info ###
alias headers='curl -I | ccze -A'
alias nettester='echo -n "black" | nc -4u -w0 localhost 1738'
alias dnsactiveservers='scutil --dns | grep 'nameserver\[[0-9]*\]''
############################
# SysMem Proc Info Aliases #
############################
## get top process eating memory
alias psmem='ps aux | sort -nr -k 4'
alias psmem10='ps aux | sort -nr -k 4 | head -10'

## get top process eating cpu ##
alias pscpu='ps aux | sort -nr -k 3'
alias pscpu10='ps aux | sort -nr -k 3 | head -10'

## Get server cpu info ##
alias cpuinfo='lscpu'
alias glances_configed='glances --config /usr/local/etc/glances/glances.conf --process-short-name --enable-process-extended --disable-quicklook --byte --diskio-iops  --diskio-show-ramfs  --fs-free-space --meangpu --enable-irq --webserver --bind localhost --port 11111 --export-graph-path "${HOME}/Logs/glances"'

########################################
#### Ethernet WiFi Specific Aliases ####
########################################
## simple defaults for eth1 if installed  ##
# alias dnstop='dnstop -l 5  eth1'
# alias vnstat='vnstat -i eth1'
# alias iftop='iftop -i eth1'
# alias tcpdump='tcpdump -i eth1'
# alias ethtool='ethtool eth1'
##############################
# work on wlan0 by default #
# Only useful for laptop as all servers are without wireless interface
# alias iwconfig='iwconfig wlan0'
# alias wget='wget -c'
############################
# End Sysadmin Aliases #####
############################

################################
#### Other Personal Aliases ####
################################
################################
### Memcached server status  ###
# alias mcdstats='/usr/bin/memcached-tool 10.10.27.11:11211 stats'
# alias mcdshow='/usr/bin/memcached-tool 10.10.27.11:11211 display'
# ## quickly flush out memcached server ##
# alias flushmcd='echo "flush_all" | nc 10.10.27.11 11211'
################################

########################################
### Get Coding Data from Wakatime API ##
########################################
alias WakatimeActivityAll='GET https://wakatime.com/share/@relic7/774bc838-5986-43eb-b118-8a4b9b7f2119.json'
alias WakatimeActivityLangMonth='GET https://wakatime.com/share/@relic7/96495b20-45be-47e1-a8d9-f6c0a7feea79.json'
alias WakatimeActivityEditors='GET https://wakatime.com/share/@relic7/8a38e0d2-b4d3-4d4f-af47-eb2fd620caf7.json'
alias WakatimeActivityLangYear='GET https://wakatime.com/share/@relic7/4feb75ba-7a9b-4326-8ed3-db72b789628d.json'
########################################
## Commit Settings to BBucket
alias SettingsGitCommit='CURRENT="$(pwd)" && cd /Users/jbragato/settings_ide_repo && git commit -a -m "sync_cron" & cd "$CURRENT" && fg'
########################################
## SSH openshift acct likely obsolete
# alias sshFlaskMongoRHCloud='ssh 58ffc0287628e148da00015a@flask-relic7.rhcloud.com'
alias subl='/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl'
alias sublime_text='/Applications/Sublime\ Text.app/Contents/MacOS/Sublime\ Text'
alias envs='env|sort'
#
### Logs
alias tailCoralOtherLogs='docker exec cs1-"${HOSTNAME}" tail -f coral/logs/rabbitmq/rabbitmq.log coral/logs/cache.log coral/logs/mongodb/mongodb.log | ccze -A'
alias tailCoralCelery='docker exec cs1-"${HOSTNAME}" tail -f coral/logs/flask/audit.log coral/logs/flask/flask.log coral/logs/celery/celery.log coral/logs/celery/default.log coral/logs/celery/local.log coral/logs/celery/router.log coral/logs/celery/subtask.log  coral/logs/celery/core.log coral/logs/celery/coral/logs/celery/important.log  coral/logs/celery/retries.log  coral/logs/celery/schedule.log  coral/logs/celery/task.log | ccze -A'
alias tailCoralNetwork='docker exec cs1-"${HOSTNAME}" tail -f coral/logs/uwsgi/uwsgi.log coral/logs/nginx/nginx.log | ccze -A'
alias tailCoralTes='docker exec cs1-"${HOSTNAME}" tail -f /data/app/tes/logfile.txt | ccze -A'
alias tailCoralTed='docker exec cs1-"${HOSTNAME}" tail -f /data/app/ted/logfile.txt | ccze -A'

############################
## Contibs mostly Bash-It ##
############################
alias dsclean='find . -type f -name .DS_Store -delete'
alias flushdnscache='dscacheutil -flushcache'
alias copyLastCmd2PB='fc -ln -1 | awk '\''{$1=$1}1'\'' ORS='\'''\'' | pbcopy'
alias qlclifile='qlmanage -p 2>/dev/null'
alias brewsr='brew search'
alias brewupc='brew update && brew upgrade --cleanup'
#
alias TunnelLocal179='ssh -L 5000:localhost:5000 em7admin@192.168.1.179 "while true; do sleep 10; echo Still Tunnelling to your MAC-BOOKPRO laptop from JBRAGATO-M-420M; done"'
alias startMongo32='/usr/local/opt/mongodb@3.2/bin/mongod --config /usr/local/etc/mongod.conf'
