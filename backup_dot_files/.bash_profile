## User Defined Environment File incl. PATH and other User Vars
#
###########################################################
########## Load .bashrc incl. shopts and aliases ##########
########## and/or .profile if in Home Dir #################
## Errors on __git_ps1 -- git bash prompt
###########################################################
#####  LOAD .profile and/or .bashrc Vars First  ###########
###########################################################
if [ -r ~/.profile ]; then . ~/.profile; fi
case "$-" in *i*) if [ -r ~/.bashrc ]; then . ~/.bashrc; fi;; esac
###############
# Load the shell dotfiles including .functions and .extra
# * ~/.path can be used to extend `$PATH`.
# * ~/.extra can be used for other settings you don’t want to commit.
# for file in ~/.{path,bash_prompt,exports,bash_aliases,bash_functions,extra}; do
#    [ -r "$file" ] && [ -f "$file" ] && source "$file";
# done;
# unset file;
###############
export PATH="/Library/Developer/CommandLineTools/usr/bin/:${PATH}"
export PATH="/usr/local/opt/coreutils/libexec/gnubin:${PATH}"
export PATH="/Users/jbragato/.mix/archives/dialyxir-1.0.0-rc.4/dialyxir-1.0.0-rc.4/ebin:${PATH}"
#
# export SHELL=/usr/local/bin/bash
## MAIN PATH DEFINITION - HOMEBREW COMPLIANT
export PATH="/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:${PATH}"
########################################
#### HOEMEBREW AND iTerm Settings ######
########################################
# iterm shellintegrqate
test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"
# Brew completion for bash shell
# if [ -f $(brew --prefix)/etc/bash_completion ]; then source $(brew --prefix)/etc/bash_completion; fi
[[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"

# [ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion
export HOMEBREW_GITHUB_API_TOKEN="930c4b25684cef7c8add1a3d25776ae859e14b29"
#########################################
##
##### Language Specific ENV VARS #######
##
#
###############################################
######  NPM NVM & NodeJS SETTINGS  ############
###############################################
#######  NPM -  Node Package Manager  #########
#######  NVM -  Node Version Manager  #########
###############################################
#
###############################################
# Homebrew Node Related - AKA - Base Node #####
########################
##   Node v.10        ##
########################
export LDFLAGS="-L/usr/local/opt/node@10/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/node@10/include ${CPPFLAGS}"
export PATH="/usr/local/opt/node@10/bin:${PATH}"
######################################
# NVM Related - Node Version Manager #
######################################
export NVM_DIR="$HOME/.nvm"
[ -s "${NVM_DIR}/nvm.sh" ] && . "${NVM_DIR}/nvm.sh"  # This loads nvm
[ -s "${NVM_DIR}/bash_completion" ] && . "${NVM_DIR}/bash_completion"  # This loads nvm bash_completion
alias nvmActivate='. ~/.nvm/nvm.sh'
#
######################################
##########  NPM related  #############
######################################
export NODE_OPTIONS='--max-old-space-size=8142'
export PATH="/usr/local/share/npm/bin:${PATH}"
export npm_config_devdir=/tmp/.gyp
export npm_config_python=`pyenv which python`
export NPM_DIR_GLOBAL="${HOME}/.npm"
# export NPM_HOME=/usr/local/lib/node_modules/npm/node_modules
########################################
#
######################################
#########  YARN related  #############
######################################
export YARNRC="${HOME}/.yarnrc"
export YARN_DIR_GLOBAL="${HOME}/.config/yarn/global"
export YARN_OFFLINE_MIRROR_CACHE_DIR="${YARN_DIR_GLOBAL}/npm-packages-offline-cache"
export WEBPACK_BIN=/usr/local/webpack-4.26.0/bin
export PATH="${WEBPACK_BIN}:${PATH}"
export PATH="${PATH}:`yarn global bin`"
#
### X11 + Xquartz
export PATH="/opt/X11/bin/:${PATH}"
#
########################################
######
# ATOM Related - APM/NPM/YARN
######
export ATOM_DIR_GLOBAL="${HOME}/.atom"
export APM_DIR_GLOBAL="${ATOM_DIR_GLOBAL}/.apm"
########################################
# Other NPM-YARN-NODE-APM/ATOM Related #
########################################
# JOBS max makes npm for node run faster
export JOBS=max
#############################################
###
#############################################
##### SSH - SSL - CA - CERTS - ETC ##########
#############################################
export SUDO_ASKPASS=/bin/systemd-ask-password
export RSA_PRIVKEY="${HOME}/.ssh/id_rsa"
export DSA_PRIVKEY="${HOME}/.ssh/id_dsa"
##############################################
####
#############################################
##### Wakeatime for Terminal/Bash ############
source /usr/local/bin/bash-wakatime.sh
#######################################
## "/Users/jbragato/virtualenvs/selenium/bin":"/Users/jbragato/virtualenvs/selenium/web_platform_tests"
##
######################################
##### General PATH STUFF #############
######################################
#
##### APACHE MAVEN and SPARK ########
export MAVEN_PATH="/usr/local/bin/apache-maven-3.6.0/bin"
export SPARK_HOME="/usr/local/bin/"
export PATH="${MAVEN_PATH}:${SPARK_HOME}:${PATH}"
####################################
#####  ARTIFACTORY CONFIG   ########
export ARTIFACTORY_HOME=/opt/artifactory
export JAVA_HOME="${JAVA_HOME}"
export JAVA_OPTIONS="${ARTIFACTORY_HOME}/bin/artifactory.default ${JAVA_OPTIONS}"
export PATH="${ARTIFACTORY_HOME}/bin:${PATH}"
###############################################
##########     GO Language      ###############
###############################################
export GOPATH="$HOME/go"
# export GOROOT="/usr/local/go/bin"
# export GOROOT="/usr/local/go/bin/go"
export PATH="${GOPATH}/bin:${GOROOT}:${PATH}"
###############################################
#
###############################################
#######   Elasticsearch SETTINGS   ############
###############################################
export ELASTIC_DATA_DIR=/usr/local/var/lib/elasticsearch/elasticsearch_jbragato/
export ELASTIC_LOGS_FILE=/usr/local/var/log/elasticsearch/elasticsearch_jbragato.log
export ELASTIC_PLUGIN_DIR=/usr/local/var/elasticsearch/plugins/
export ELASTIC_CONFIG_DIR=/usr/local/etc/elasticsearch/
###############################################
##############################################################################################
##############################################
#### Build Requirement Environmental Vars #### ###############################################
##############################################
############################################### ##############################################
##  Additional CPP and LD Compilers  ##########
###############################################
export CPPFLAGS="-I/usr/local/opt/openssl/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/openssl/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/openssl@1.1/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/openssl@1.1/lib ${LDFLAGS}"
export LDFLAGS="-L/usr/local/opt/openblas/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/openblas/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/net-snmp/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/net-snmp/include ${CPPFLAGS}"
#
export LDFLAGS="-L/usr/local/opt/bison/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/qt/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/qt/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/nss/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/nss/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/icu4c/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/icu4c/lib ${LDFLAGS}"
#export CPPFLAGS="-I/usr/local/opt/gettext/include ${CPPFLAGS}"
#export LDFLAGS="-L/usr/local/opt/gettext/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/ossp-uuid/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/ossp-uuid/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/yara/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/yara/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/libmagic/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/libmagic/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/libffi/include ${CPPFLAGS}"
export CPPFLAGS="-I/usr/local/opt/libffi/lib/libffi-3.2.1/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/libffi/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/libmaxminddb/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/libmaxminddb/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/krb5/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/krb5/lib ${LDFLAGS}"
export LDFLAGS="-L/usr/local/opt/libpcap/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/libpcap/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/readline/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/readline/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/libxml2/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/libxml2/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/sqlite/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/sqlite/include ${CPPFLAGS}"
export LDFLAGS="-L/usr/local/opt/util-linux/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/util-linux/include ${CPPFLAGS}"
# export CPPFLAGS="-I/usr/local/opt/glib/include ${CPPFLAGS}"
# export CPPFLAGS="-I/usr/local/opt/glib/lib/glib-2.0/include ${CPPFLAGS}"
# export LDFLAGS="-L/usr/local/opt/glib/lib ${LDFLAGS}"
# Open LDAP
export LDFLAGS="-L/usr/local/opt/openldap/lib ${LDFLAGS}"
export CPPFLAGS="-I/usr/local/opt/openldap/include ${CPPFLAGS}"
###############################
###### PKG_CONFIG_PATH   ######
###############################
export PKG_CONFIG_PATH="/usr/local/opt/qt/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PKG_CONFIG_PATH="/usr/local/opt/openssl@1.1/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PKG_CONFIG_PATH="/usr/local/opt/nss/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PKG_CONFIG_PATH="/usr/local/opt/icu4c/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PKG_CONFIG_PATH="/usr/local/opt/krb5/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PKG_CONFIG_PATH="/usr/local/opt/libpcap/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PKG_CONFIG_PATH="/usr/local/opt/openblas/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PKG_CONFIG_PATH="/usr/local/opt/readline/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PKG_CONFIG_PATH="/usr/local/opt/libxml2/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PKG_CONFIG_PATH="/usr/local/opt/sqlite/lib/pkgconfig:${PKG_CONFIG_PATH}"
export PKG_CONFIG_PATH="/usr/local/opt/util-linux/lib/pkgconfig:${PKG_CONFIG_PATH}"
#export PKG_CONFIG_PATH="/usr/local/opt/libffi/lib/pkgconfig:${PKG_CONFIG_PATH}"
#export PKG_CONFIG_PATH="/usr/local/opt/glib/lib/pkgconfig:${PKG_CONFIG_PATH}"
####
###############################
#    Special Build Cases      #
###############################
###### LibFFI               ###
###############################
export LIBFFI_LIBS="/usr/local/opt/libffi/lib -lffi"
export LIBFFI_CFLAGS="/usr/local/opt/libffi/lib/include"
#
######################################
##### General PATH STUFF #############
######################################
## Related to LD CPP and PKG Config Above
export PATH="/usr/local/opt/glib/bin:${PATH}"
export PATH="/usr/local/opt/gettext/bin:${PATH}"
export PATH="/usr/local/opt/yara/bin:${PATH}"
export PATH="/usr/local/opt/ossp-uuid/bin:${PATH}"
export PATH="/usr/local/opt/openldap/bin:${PATH}"
export PATH="/usr/local/opt/openldap/sbin:${PATH}"
export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:${PATH}"
export PATH="/usr/local/opt/gnu-getopt/bin:${PATH}"
export PATH="/usr/local/opt/qt/bin:${PATH}"
export PATH="/usr/local/opt/nss/bin:${PATH}"
export PATH="/usr/local/opt/bison/bin:${PATH}"
export PATH="/usr/local/opt/openssl/bin:${PATH}"
export PATH="/usr/local/opt/openssl@1.1/bin:${PATH}"
export PATH="/usr/local/opt/curl-openssl/bin:${PATH}"
export PATH="/usr/local/opt/libpq/bin:${PATH}"
export PATH="/usr/local/opt/curl/bin:${PATH}"
export PATH="/usr/local/opt/libpq/bin:${PATH}"
export PATH="/usr/local/opt/m4/bin:${PATH}"
export PATH="/usr/local/opt/net-snmp/bin:${PATH}"
export PATH="/usr/local/opt/net-snmp/sbin:${PATH}"
export PATH="/usr/local/opt/libxml2/bin:${PATH}"
export PATH="/usr/local/opt/krb5/bin:${PATH}"
export PATH="/usr/local/opt/krb5/sbin:${PATH}"
export PATH="/usr/local/opt/util-linux/bin:${PATH}"
export PATH="/usr/local/opt/util-linux/sbin:${PATH}"
#
############################################################
###  OTHER PATH ADDITIONS DBs - Mongo Mysql and Postgres  ##
############################################################
#
export PATH="/Applications/MongoDB.app/Contents/Resources/Vendor/mongodb/bin:${PATH}"
export PATH="/usr/local/opt/postgresql@9.6/bin:${PATH}"
export PATH="/usr/local/bin/mysql:/usr/local/opt/mysql@5.7/bin:/usr/local/opt/mysql-client/bin:${PATH}"
export PATH="/usr/local/opt/sqlite/bin:${PATH}"
############################################################
#
export PATH="/Users/jbragato/virtualenvs/VIRTUAL_CORALENV_PYTHON/bin/crucible-fisheye:${PATH}"
#
export PATH="/Users/jbragato/.cabal/bin:${PATH}"
#### Add Local user bin if available - incl user scripts + apps
export PATH="/Users/jbragato/bin:${PATH}"
# /Users/jbragato/.rubies/ruby-2.5.0/bin:/usr/local/opt/python/libexec/bin:/Users/jbragato/bin:/Users/jbragato/Library/Python/2.7/bin:/usr/local/opt/openssl/bin:/usr/local/bin/mysql:/usr/local/sbin:/usr/local/bin:/usr/local/opt/sqlite/bin:/Users/jbragato/.rubies/ruby-2.4.0/bin:/usr/local/opt/bison/bin:/usr/local/opt/libpq/bin:/usr/local/opt/openssl/bin:/Applications/MongoDB.app/Contents/Resources/Vendor/mongodb/bin:/usr/local/opt/python/libexec/bin:/Users/jbragato/bin:/Users/jbragato/Library/Python/2.7/bin:/usr/local/opt/openssl/bin:/usr/local/bin/mysql:/usr/local/sbin:/usr/local/bin:/usr/local/opt/sqlite/bin:/usr/local/opt/mysql@5.7/bin:/usr/local/opt/libpq/bin:/usr/local/opt/openssl/bin:/Applications/MongoDB.app/Contents/Resources/Vendor/mongodb/bin:/usr/local/opt/python/libexec/bin:/usr/local/sbin:/usr/local/bin:/usr/local/opt/sqlite/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/MacGPG2/bin:/opt/X11/bin:/Library/Frameworks/Mono.framework/Versions/Current/Commands:/Users/jbragato/bin:/Users/jbragato/Library/Python/2.7/bin:/usr/local/opt/openssl/bin:/usr/local/mysql/bin:${PATH}"
###############################
#
###############################
###### ICU4c Issues          ##
###############################
export PATH="/usr/local/opt/icu4c/bin:${PATH}"
export PATH="/usr/local/opt/icu4c/sbin:${PATH}"
#
# export NODEBREW_ROOT=/usr/local/var/nodebrew
### ZMQ Related
export XML_CATALOG_FILES="/usr/local/etc/xml/catalog"
#
#########################################
######### PYTHON Related ################
#########################################
export PATH="$(brew --prefix)/opt/python/libexec/bin:${PATH}"
## Anaconda - conda tab-completion
# eval "$(register-python-argcomplete conda)"
#

################################################
###### VIRTUALENV + WRAPPER RELATED SETUP #######
#################################################
export PYENV_VIRTUALENV_DISABLE_PROMPT=1
# export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"
#
# Needed for virtualenvwrapper.sh if used
export VIRTUALENVWRAPPER_PYTHON=python
export WORKON_HOME="~/virtualenvs"
export PROJECT_HOME="${WORKON_HOME}/projects"
###########################################
# Pipenv - Virtualenv PATH resolution helper #
export PIPENV_VENV_IN_PROJECT=1
########################################
###### PYENV RELATED SETUP #############
########################################
export PYENV_ROOT="${HOME}/.pyenv"
export PATH="${PYENV_ROOT}/shims:/usr/local/Cellar/pyenv-virtualenvwrapper/20140609/bin/:${PATH}"
export PATH="${PYENV_ROOT}/bin:${PATH}"

eval "$(pyenv init -)";


if "$(which pyenv-virtualenv-init)" > /dev/null; 
 then eval "$(pyenv-virtualenv-init -)"; 
fi;


# export PYTHONPATH="/usr/local/lib/python2.7/site-packages:${PYTHONPATH}"
# pip bash completion start
_pip_completion()
{
COMPREPLY=( $( COMP_WORDS="${COMP_WORDS[*]}" \
               COMP_CWORD=$COMP_CWORD \
               PIP_AUTO_COMPLETE=1 $1 ) )
}
complete -o default -F _pip_completion pip
# pip bash completion end

###########################################
####### End Python, Pyenv, Related ########
###########################################
#######
#######
###########################################
####### MongoDb Dependent Env Setup #######
###########################################
export MONGO_PATH="/usr/local/opt/mongodb@3.2"
export PATH="${PATH}:${MONGO_PATH}/bin"
###########################################
####### End MONGODB Dependencies   ########
###########################################
#
##################################################################
############### End PATH Settings except Ruby/RVM/GEM ####################
##################################################################
##
# Finally The Below Ruby/RVM must be at end of Profile
##
#######################################################
######   RUBY, RVM, GEMS VERSION Manage  ##############
#######################################################
# Add Ruby Version Manager -- RVM to PATH for scripting.
# *** Make sure this is the last PATH variable change.
# export GEM_PATH="${HOME}/.rvm/rubies/ruby-2.5.1/bin/gem"
#export GEM_PATH="${HOME}/.rvm/gems/default/gems"
export PATH="${HOME}/.rvm/sripts/rvm/rubies:${PATH}"
export PATH="${PATH}:${HOME}/.rvm/bin"

# export PATH=${HOME}/.rvm/sripts/rvm/rubies:${PATH}"
# This Will Load RVM into a shell session *as a function*and Source the Environ which sets GEM_PATH and HOME
[[ -s "${HOME}/.rvm/scripts/rvm" ]]
# Avoiding GEM_PATH warnings
export rvmsudo_secure_path=0
#source "${HOME}/.rvm/scripts/rvm" && source "${HOME}/.rvm/environments/default" ;
#source /etc/profile.d/rvm.sh
#
#
##################
    ## Finally ##
############################
#### Dedupe PATHs At End ###
############################
## PKG_CONFIG_PATH
XPKGPATH="$(printf "%s" "${PKG_CONFIG_PATH}" | awk -v RS=':' '!a[$1]++ { if (NR > 1) printf RS; printf $1 }')"
PKG_CONFIG_PATH="${XPKGPATH}"
# LDFLAGS
XLDPATH="$(printf "%s" "${LDFLAGS}" | awk -v RS=' ' '!a[$1]++ { if (NR > 1) printf RS; printf $1 }')"
LDFLAGS="${XLDPATH}"
# CPPFLAGS
XCPPFLG="$(printf "%s" "${CPPFLAGS}" | awk -v RS=' ' '!a[$1]++ { if (NR > 1) printf RS; printf $1 }')"
CPPFLAGS="${XCPPFLG}"
# or use . ~/.bash_build_flags -- not implemented just an idea for separation from PATH related processes
############################
#### Dedupe PATH         ###
############################
# PATH #
########
XPATH="$(printf "%s" "${PATH}" | awk -v RS=':' '!a[$1]++ { if (NR > 1) printf RS; printf $1 }')"
PATH="${XPATH}"
## Fin

