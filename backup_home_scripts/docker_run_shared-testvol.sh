#!/bin/bash
#
# ensure running as root
if [ "$(id -u)" != "0" ]; then
  exec sudo "$0" "$@"
fi
# Run as daemon with -d
case "$1" in
    -d|--daemon)
        $0 < /dev/null &> /dev/null & disown
        exit 0
        ;;
    *)
        ;;
esac

SHARED_VOL=${3:-DockerShare1}
docker container rm -f cs1-${HOSTNAME} ;
docker volume rm -f ${SHARED_VOL} ;
docker volume create ${SHARED_VOL} ;

echo "Docker Run $(date)"
docker run -d --rm --name cs1-$(hostname) --hostname cs1-$(hostname) -v DockerShare1:/coral-services --add-host=splunk-head-deployment-server:10.201.149.149 \
--add-host=splunk-momidx-01:10.201.149.149 --add-host=splunk-momidx-02:10.201.149.149 -v /data01/commands:/data \
-p 5000:5000 -p 27017:27017 -p 6000:6000 -p 5671:5671 -p 4369:4369 -p 25672:25672 -p 3000:15672 -p 8888:8888 --env-file /opt/commands/env.list coral-services:devjupyter

sleep 12 ;
#
echo "Docker Permission Fix $(date)"
#
$(sudo chown -R root:docker /var/lib/docker/volumes /var/lib/docker/volumes/${SHARED_VOL} /var/lib/docker/volumes/${SHARED_VOL}/_data  ; sudo chmod 775 /var/lib/docker/ /var/lib/docker/volumes/ ; sudo chmod -R 775 /var/lib/docker/volumes/${SHARED_VOL} /var/lib/docker/volumes/${SHARED_VOL}/_data;) & 
#
## Move the .idea settings to the container volume
echo "Pycharm .idea cp $(date)"
sleep 4 ;
## Sync diff from repo dir to DockerShare1
/usr/bin/rsync --compress --update --exclude *.git/ --verbose /home/jbragato/virtualenvs/coral-services/ /var/lib/docker/volumes/DockerShare1/_data ;
# $(sudo cp -R /home/jbragato/virtualenvs/coral-services/.idea /var/lib/docker/volumes/${SHARED_VOL}/_data/  ; sudo chown -R root:docker /var/lib/docker/volumes/${SHARED_VOL}/_data/.idea/ ; sudo chmod -R 775 /var/lib/docker/volumes/${SHARED_VOL}/_data/.idea;) &

echo -e ${USER} ;
echo "DONE $(date)" ;
echo "Container cs1-${HOSTNAME} launched";
echo -e "Container Live Files in:\n\t /var/lib/docker/volumes/${SHARED_VOL}"

