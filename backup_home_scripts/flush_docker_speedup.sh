#!/usr/local/bin/bash


cd ~/Library/Containers/com.docker.docker/Data/database/
git reset --hard
cat com.docker.driver.amd64-linux/disk/full-sync-on-flush
echo false > com.docker.driver.amd64-linux/disk/full-sync-on-flush
git add com.docker.driver.amd64-linux/disk/full-sync-on-flush
git commit -s -m "Disable flushing"
