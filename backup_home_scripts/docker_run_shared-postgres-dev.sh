#!/bin/bash


export POSTGRES_PASSWD=docker
export POSTGRES_CONTAINER=postgres-dev
export POSTGRES_PORT=5432

docker run --rm -d -i -t --privileged \
	--name "$POSTGRES_CONTAINER" \
	--hostname "$POSTGRES_CONTAINER" \
	-p "$POSTGRES_PORT":"$POSTGRES_PORT" \
	-e POSTGRESQL_USER="$POSTGRES_PASSWD" \
	-e POSTGRESQL_PASS="$POSTGRES_PASSWD" \
	-e POSTGRESQL_DB="$POSTGRES_PASSWD" \
	relic7/postgres-dev:9.3



# CREATE TABLE public.employees   (
#     emp_no SERIAL PRIMARY KEY,
#     first_name VARCHAR(50),
#     last_name VARCHAR(70),
#     gender VARCHAR(1),
#     hire_date DATE
# );
# INSERT INTO public.employees (
#     emp_no,
#     first_name,
#     last_name,
#     gender,
#     hire_date) 
#     VALUES (1, 'Tom', 'Mitson', 'M', '2018-01-02');