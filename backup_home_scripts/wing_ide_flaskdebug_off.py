from flask import Flask
app = Flask(__name__)

...

if __name__ == "__main__":
    import os
    if 'WINGDB_ACTIVE' in os.environ:
        app.debug = False
    app.run()

