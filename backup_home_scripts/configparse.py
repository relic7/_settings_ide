#!/usr/bin/env python
# coding: utf-8

import datetime, uuid
import json, yaml
import sys, argparse
from os import walk, path, rename, makedirs, getcwd, chdir
from ast import literal_eval
from collections import defaultdict, OrderedDict

if int(sys.version[0]) > 2:
    from configparser import ConfigParser
else:
    from ConfigParser import ConfigParser


def uuid4_hex():
    return uuid.uuid4().hex


def recursive_dirlist(rootdir, file_query='config.ini'):
    """
         Walk Root Directory and Return List or all Files in all
         Subdirs too and renames spec to dir name and app_component
         :param rootdir - path to dpp project tree root directory
         :param file_query - file or file pattern to be searched for and parsed
         :return list - of unique filepaths
    """
    walkedlist = []
    for dirname, subdirnames, filenames in walk(rootdir):
        # append path of all filenames to walkedlist
        # if not '.git' in dirname.split('/'):
        if filenames:
            for filename in filenames:
                file_path = path.abspath(path.join(dirname, filename))
                if path.isfile(file_path) and path.basename(file_path) == file_query:
                        walkedlist.append(file_path)

    walkedset = list(set(sorted(walkedlist)))
    return walkedset


def json_dumps_minified(obj):
    """
        Cleans up the parsed config.ini Dictionary for use during import
        :param dictionary of parsed config.ini file (configfile_to_dict)
        :return string - json.dumps string to be loaded as dict
    """
    settings_json = {
        "use_entire_file_if_no_selection": True,
        "indent": 2,
        "sort_keys": True,
        "ensure_ascii": True,
        "line_separator": ",",
        "value_separator": ": ",
        "keep_arrays_single_line": True,
        "max_arrays_line_length": 250,
        "pretty_on_save": True,
        "validate_on_save": True
    }

    line_separator = settings_json.get("line_separator", ",")
    value_separator = settings_json.get("value_separator", ": ")

    sort_keys = settings_json.get("sort_keys", False)

    return json.dumps(obj,
                      ensure_ascii=settings_json.get("ensure_ascii", False),
                      sort_keys=sort_keys,
                      separators=(line_separator.strip(), value_separator.strip())
                      )


def configsparsed(rootdir, **kwargs):
    """
        Call the above functions to return output from configs for parsing into
        proper mongodb collections
        :param directory - root dir path to the dpp file tree
        :returns json struct of configs - ready to be parsed into collections
    """
    parsed_configs = []
    configfiles = recursive_dirlist(rootdir, file_query='config.ini')
    for f in configfiles:
        parsed_configs.append(configfile_to_dict(f))
    return json_dumps_minified(parsed_configs)


def define_lookup_dictionaries_global(**kwargs):
    # Make Lookup Dicts by name/or guid as key
    lk_up_evp_name = json.loads(open(kwargs.get('lookupfile'), 'rb').read())
    #
    lookupdict_by_name = defaultdict(list)
    lookupdict_by_guid = defaultdict(list)
    #
    for col in lk_up_evp_name:
        lookupdict_by_name[col.get('event_policy_name')] = col.get('event_policy_guid')
        lookupdict_by_guid[col.get('event_policy_guid')] = col.get('event_policy_name')
    #globals()['lookupdict_by_name'] = lookupdict_by_name
    #globals()['lookupdict_by_guid'] = lookupdict_by_guid
    return lookupdict_by_guid, lookupdict_by_name


def main(**kwargs):
    lookupdict_by_guid, lookupdict_by_name = define_lookup_dictionaries_global(lookupfile=kwargs.get('lookupfile'))
    configfile = kwargs.get('configfile')
    configs_parsed = []
    if type(configfile) == list and len(configfile) > 1:
        # multifiles to parse
        for f in kwargs.get('configfile'):
            parsed_file = {}
            parsed_file[f] = configsparsed(f)
            configs_parsed.append(parsed_file)
    else:
        # single file passed to parse
        configs_parsed.append(configsparsed(configfile))

    print(locals().keys(), kwargs.keys())





    pass


#######################################################################################
# Cmdline usage and arguments allowed       ###########################################
#######################################################################################
parser = argparse.ArgumentParser(
                    description='Parse config.ini Files or any ini for that matter\
                    by section block name fragment, then output \
                    or further parse by section\'s options. \
                    The diagnostic-power-packs git repo tree is assumed to be in \
                    the current directory or in the users home \
                    directory, under a subdirectory named "repositories". \
                    ex. "~/repositories/diagnostic-power-packs/CiscoXE/config.ini"' 
                    )
# Basic Args - for script to parse and additional data for further querying
parser.add_argument('-f', '--configfile', 
                    metavar='FILEPATH',
                    help='The full absolute path to the config.ini to be parsed')
parser.add_argument('-l', '--lookupfile', 
                    metavar='LOOKUPFILE',
                    default='event_name_guid_map_pp33_utf8.json',
                    help='Choose a json file to use for additional matching of section and option values \
                         .ie richards lookupfile of event policy name --> event policy guid')
# Outout Args
parser.add_argument('-d', '--directory',
                    metavar='BASEDIRPATH',
                    default=getcwd(),
                    help='All Files using relative paths will be referenced from the current directory, \
                    unless this flag is explicitly set. \
                    If this option is set, the diagnostic-power-packs git repo tree must be \
                    located in the base of that defined directory.')
parser.add_argument('-r', '--returnfile', 
                    metavar='RETURN',
                    default='parsed_output.txt',
                    help='Choose to retrurn output to a file or stdout for further processing \
                    If a file is included. Value should be in the form of complete full path')
parser.add_argument('-y', '--yaml', 
                    metavar='YAMLOUTPUT',
                    help='Choose to retrurn output as formatted yaml files separated in \
                    directory hierarchy of TE/Enrichments repository on bitbucket. \
                    ex. "enrichments/yaml/action"')
# Parsing Args - Config Sections or Options to Lookfor
parser.add_argument('-s', '--section', 
                    metavar="SECTION", 
                    default='policy',
                    help='Config section full or partial -- \
                    "policy:<policy_name>" or \
                    "action:<action_name>"')
parser.add_argument('-o', '--option', 
                    metavar='OPTION', 
                    default="allignedevents",
                    help='The field values to return from the defined section')
#######################################################################################
# relative parent dir paths to all currently blacklisted directories not to be searched
#######################################################################################
current_blacklisted_directories = [ 
  'cisco_cms_helper', 
  'examples', 
  'fw_docs', 
  'docs', 
  'cisco_cms_jinja2', 
  'cisco_cms_utils', 
  'cisco_cms_markupsafe', 
  'cisco_cms_work_instructions', 
  'cisco_cms_nxtgen',  
  '.git', 
  'tools',
  'PowerPacks',
  'ContentLibraries',
  '.ipynb_checkpoints',
  '.idea', 
  'DynamicApplications'
]


if __name__ == '__main__':
    ### for testing only
    chdir('/Users/jbragato/virtualenvs/Migrate2_2019_DPP/')
    ###
    YAML_ACTION_OUT = 'enrichments/yaml/action'
    YAML_ENRICHMENT_OUT = 'enrichments/yaml/enrichment'
    rootdir="diagnostic-power-packs"
    # Kwargs to pass to main(**kwargs)
    args = parser.parse_args()
    keyword_args =  defaultdict(list)
    # BASEDIR flag used to define abspath - basedir & rootdir
    if args.directory:
        basedir = path.join(path.abspath(args.directory))
        rootdir = path.join(basedir, rootdir)
    else:
        basedir = path.abspath(getcwd())
        rootdir = path.join(basedir, rootdir)
        # basedir = path.join(path.expanduser('~'), 'repositories')
    keyword_args['basedir'] = basedir
    keyword_args['rootdir'] = rootdir
    #
    # Abspaths to all currently known config.ini files valid to parse
    current_configfiles_whitelisted =[ 
        path.join(basedir, rootdir, 'CiscoXE/config.ini'),
        path.join(basedir, rootdir, 'NetApp/config.ini'),
        path.join(basedir, rootdir, 'Poller/config.ini'),
        path.join(basedir, rootdir, 'F5/config.ini'),
        path.join(basedir, rootdir, 'CiscoIOS/Ethernet/config.ini'),
        path.join(basedir, rootdir, 'VMware/config.ini'),
        path.join(basedir, rootdir, 'System/config.ini'),
        path.join(basedir, rootdir, 'ScreenOS/config.ini'),
        path.join(basedir, rootdir, 'Tetration/config.ini'),
        path.join(basedir, rootdir, 'CiscoACI/config.ini'),
        path.join(basedir, rootdir, 'CSP/config.ini'),
        path.join(basedir, rootdir, 'Viptela/config.ini'),
        path.join(basedir, rootdir, 'CiscoNSO/config.ini'),
        path.join(basedir, rootdir, 'NetSNMP/config.ini'),
        path.join(basedir, rootdir, 'CiscoXR/config.ini'),
        path.join(basedir, rootdir, 'Tandberg/config.ini'),
        path.join(basedir, rootdir, 'CiscoOLT/config.ini'),
        path.join(basedir, rootdir, 'CiscoIOS/config.ini'),
        path.join(basedir, rootdir, 'JunOS/config.ini'),
        path.join(basedir, rootdir, 'CiscoIOS/RoutingProtocols/config.ini'),
        path.join(basedir, rootdir, 'CiscoME/config.ini'),
        path.join(basedir, rootdir, 'config.ini'),
        path.join(basedir, rootdir, 'Windows/config.ini'),
        path.join(basedir, rootdir, 'CiscoUCS/config.ini'),
        path.join(basedir, rootdir, 'CiscoNXOS/config.ini'),
        path.join(basedir, rootdir, 'CiscoACE/config.ini'),
        path.join(basedir, rootdir, 'CiscoIOS/Sensors/config.ini'),
        path.join(basedir, rootdir, 'StarOS/config.ini'),
        path.join(basedir, rootdir, 'Wireless/config.ini'),
        path.join(basedir, rootdir, 'NetScaler/config.ini')
    ]
    #
    # Config.ini file Abspath - configfile
    if args.configfile:
        if path.isfile(args.configfile):
            configfile = args.configfile
        elif path.abspath(path.isfile(args.configfile)):
            configfile = path.abspath(args.configfile)
        else:
            configfile = current_configfiles_whitelisted
    else:
        configfile = current_configfiles_whitelisted
    keyword_args['configfile'] = configfile
    # Lookupfile Abspath - lookupfile
    if args.lookupfile:
        print(args.lookupfile)
        if path.isfile(path.abspath(args.lookupfile)):
            lookupfile = path.abspath(args.lookupfile)
        elif path.isfile(path.abspath(path.join(basedir, args.lookupfile))):
            lookupfile = path.abspath(path.join(basedir, args.lookupfile))
        keyword_args['lookupfile'] = lookupfile
    # If YAML flag set - create directory hierarchy for output
    if args.yaml:
        yamldirs = []
        for d in [YAML_ACTION_OUT, YAML_ENRICHMENT_OUT]:
            yaml_dirpath = path.join(basedir, d)
            try:
                makedirs(yaml_dirpath)
                yamldirs.append(yaml_dirpath)
            except:
                pass
        keyword_args['yaml_output_dirs'] = yamldirs

    # Run with properly defined kwargs
    main(**keyword_args)


