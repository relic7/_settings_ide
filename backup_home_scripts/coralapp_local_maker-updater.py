#!/usr/local/bin/python

from pymongo import MongoClient
from pprint import pprint
import argparse
import os, shutil
from bson.objectid import ObjectId
import json

"""
#### This will install a config file to the users ~/.config dir on first run
#### Future calls of script will read the ~/.config/mongo_coral_config.json
# Check if a mongo config file exists in ~/.config/mongo_coral_config.json
# and set it as the default for the arg parser
# If the file doesnt exist in users ~/.config dir
#     write the default config.json in this scripts dir
#     to the users ~/.config dir, represented in the code as config_filepath
#### This is only setting the default and the -c flag can overide
#### the default config file at runtime by passing -c with a different json file
"""
## Configuration Default Setup - No need to provide config at runtime
##############################################################################
## --> But config.json file must be customized if not using 127.0.0.1 to connect to mongo
##############################################################################
config_filepath = os.path.join(os.getenv('HOME'), '.config/mongo_coral_config.json')
if not os.path.exists(config_filepath):
    try:
        os.makedirs(os.path.join(os.getenv('HOME'), '.config'))
    except:
        pass
    config_def = '%s/config.json' % os.path.dirname(os.path.realpath(__file__))
    with open(config_def) as json_data_file:
        config_data = json.load(json_data_file)
    with open(config_filepath, 'w') as cfile:
        cfile.write(config_data)
####
# Lets also install this script in your local ~/bin directory for later user
if not os.path.exists(os.path.join(os.getenv('HOME'), 'bin')):
    os.makedirs(os.path.join(os.getenv('HOME'), 'bin'))
    shutil.copy2(os.path.realpath(__file__), os.path.join(os.getenv('HOME'), 'bin'))
    print("The Mongo Config file and this current script are now install to \
          ${HOME}/.config/ and ${HOME}/bin, respectively. You should make sure your \
          $PATH includes the ${HOME}/bin dir for convenient running of this script")

# Now with those installed we'll parse the cmd line args
# in case there are changes to the defaults
parser = argparse.ArgumentParser(
                    description='Create AppDir Structure for Coral Apps and \
                    bi-directionally sync files to App\'s own MongoDB'
                    )
parser.add_argument('-n', '--name', metavar="APP_NAME",
                    help='Coral App name -- "Case Sensitive"', required=True)
parser.add_argument('-m', '--mode', metavar='MODE', help='\"install - if \
                     initial setup\" or \"get or pull - get update from db\" \
                     or \"put or push - put update to the db\"', default="get")
parser.add_argument('-c', '--config', metavar='MongoDB_Configfile',
                    default=config_filepath,
                    help='Choose a different config file \
                    (default:<gitroot>/Tools/coralapp_local_maker-updater/config.json')
parser.add_argument('-f', '--file', metavar='FILEPATH',
                    help='This Flag should be used if \
                    passing a single file for update, rather that the BASEDIR')
parser.add_argument('-d', '--directory',
                    metavar='BASEDIRPATH',
                    default=os.getcwd(),
                    help='Base directory for the app on the local filesystem \
                           (default: current directory ). ------------------- \
                    This directory will hold the root dir("Manage") and the app component_type subdirs, structured like so: \
                    __:"YourProjectsBaseDirectory(os.getcwd())" \
                    _____:"Manage<root_dir>" -note: CreatedIfNotExists   \
                    ________:AppComponentsTypes(Directories) .ie AppModule \
                    ___________:AppComponentCode(Files(.py/.html)) \
                    ::please_note:: all component dirs will contain an __init__.py for import recognition by IDE)')
args = parser.parse_args()

###
# Finally loading args.config for use by script/client
config = json.loads(open(args.config, 'rb').read())


client = MongoClient('mongodb://{host}:{port}/'.format(
                                                host=config['mongo']['host'],
                                                port=config['mongo']['port']))
db = getattr(client, config['mongo']['db'])
db.logout()
db.authenticate(config['mongo']['username'],
                password=config['mongo']['password'])


# Sets the AppPath to Manage, so the components are placed there,
# AppName should be the app's name passed in with -n "Case Sensitive"
BASE_FOLDER = os.path.dirname(args.directory)
APP_NAME = args.name
APP_PATH = os.path.join(BASE_FOLDER, APP_NAME, "Manage")


# type = how mongodb sees the component type
# collection name = how the CoralCore DB stores them in mongo
# type_name = How Coral App UI and your App's Code sees them
COMPONENT_TYPES = [
    {"type": "cli_callbacks", "collection_name": "app_callback", "type_name": "AppCliCallback"},
    {"type": "collections", "collection_name": "app_collection", "type_name": "AppCollection"},
    {"type": "device_callbacks", "collection_name": "app_callback", "type_name": "AppDeviceCallback"},
    {"type": "discovery_callbacks", "collection_name": "app_callback", "type_name": "AppDiscoveryCallback"},
    {"type": "dispatchers", "collection_name": "app_callback", "type_name": "AppDispatcher"},
    {"type": "event_callbacks", "collection_name": "app_callback", "type_name": "AppEventCallback"},
    {"type": "events", "collection_name": "event", "type_name": "AppEvent"},
    {"type": "forms", "collection_name": "app_form", "type_name": "AppForm"},
    {"type": "modules", "collection_name": "app_module", "type_name": "AppModule"},
    {"type": "plugins", "collection_name": "app_plugin", "type_name": "AppPlugin"},
    {"type": "schedules", "collection_name": "app_schedule", "type_name": "AppSchedule"},
    {"type": "snmp_callbacks", "collection_name": "app_callback", "type_name": "AppSnmpCallback"},
    {"type": "standalone_callbacks", "collection_name": "app_callback", "type_name": "AppStandaloneCallback"},
    {"type": "webhooks", "collection_name": "app_webhook", "type_name": "AppWebhook"},
    {"type": "webpages", "collection_name": "app_webpage", "type_name": "AppWebpage"},
    {"type": "webviews", "collection_name": "app_webview", "type_name": "AppWebview"}]


def write_to_file(data, type_name, filename):
    fullpath = os.path.join(APP_PATH, type_name, filename)
    with open(fullpath, "w") as this_file:
        print("Creating file %s" % fullpath)
        this_file.write(data)


def db_to_files():
    for comp_type in COMPONENT_TYPES:
        name = comp_type['type']
        type_name = comp_type['type_name']
        collection = comp_type['collection_name']
        print("Looking for %s" % name)
        for component_id in app[name]:
            print("Found {}".format(component_id))
            this_component = db[collection].find_one({"_id": ObjectId(component_id)})

            try:
                spec = this_component['spec']
            except:
                spec = None

            try:
                template = this_component['template']
            except:
                template = None

            if not spec or not template:
                write_to_file("", type_name, "__init__.py")
            else:
                if name in ['webpages']:
                    write_to_file(spec, type_name, this_component['name'] + ".html")
                elif name in ['webviews']:
                    write_to_file(template, type_name, this_component['name'] + ".html")
                    write_to_file(spec, type_name, this_component['name'] + ".py")
                    write_to_file("", type_name, "__init__.py")
                else:
                    # Write __init__.py so that other python components can import them
                    write_to_file(spec, type_name, this_component['name'] + ".py")
                    write_to_file("", type_name, "__init__.py")

        print("-----------------------\n")


def files_to_db():
    """
    Updates the mongodb with changes in either the entire project dir or just
    a single components file if -f flag used
    """
    for comp_type in COMPONENT_TYPES:
        name = comp_type['type']
        type_name = comp_type['type_name']
        collection = comp_type['collection_name']
        folder = os.path.join(APP_PATH, type_name)
        # Remove __init__.py files as they are not needed by Coral only External IDE
        # --> And if the -f flag is set then only the selected file will update
        try:
            filepath = os.path.abspath(args.file)
            if filepath and os.path.exists(filepath):
                folder = os.path.abspath(os.path.dirname(filepath))
                # Set the single file int he update list
                files_for_update = [filepath]
                print("Only Updating %s..." % filepath)
        except:

            print("Looking at %s..." % folder)
            # Look for files inside each folder
        files_for_update = [fname for fname in os.listdir(folder)
                                if fname != '__init__.py']

        print("Found these files to check: %s..." % files_for_update)
        for filename in files_for_update:
            print("Checking if there were changes to %s..." % filename)
            with open(os.path.join(folder, filename), "r") as this_file:
                try:
                    this_component_id = db[collection].find_one({"app_id": str(app["_id"]), "name": os.path.splitext(filename)[0]})['_id']
                    this_component = db[collection].find_one({"_id": ObjectId(this_component_id)})
                    if name in ['webviews'] and os.path.splitext(filename)[1] == ".html":
                        this_template = this_file.read()
                        if this_component['template'] != this_template:
                            print("Yep, there were changes to the template %s." % filename)
                            # Update route_targets
                            db.app.update({"_id": app["_id"], "route_targets.id": str(this_component_id)},
                                          {"$set": {"route_targets.$.template": this_template}})

                            # Now modify the actual collection for the component
                            db[collection].update(
                                {"_id": this_component_id},
                                {
                                    "$set": {
                                        "template": this_template
                                    }
                                }
                            )

                            print("Updated {component}.\n".format(component=this_component_id))
                        else:
                            print("No changes to {component}\n".format(component=this_component_id))

                    else:
                        this_spec = this_file.read()
                        if this_component['spec'] != this_spec:
                            print("Yep, there were changes to %s." % filename)
                            this_component['spec'] = this_spec
                            # If we're dealing with webhooks, webviews or webpages,
                            # we may need to modify the spec in the main app collection directly TOO
                            # (it's an embedded document):
                            if name in ['webpages', 'webhooks']:
                                db.app.update({"_id": app["_id"], "route_targets.id": str(this_component_id)},
                                                       {"$set": {"route_targets.$.template": this_spec}})

                            # Now modify the actual collection for the component
                            db[collection].update(
                                {"_id": this_component_id},
                                {
                                    "$set": {
                                        "spec":this_spec
                                    }
                                }
                            )

                            print("Updated {component}.\n".format(component=this_component_id))
                        else:
                            print("No changes to {component}\n".format(component=this_component_id))
                except:
                    print("Probably this component doesn't exist. Ignoring.\n")
    print("DONE")


def install():
    """ MODE Flag(-m) with Install, added setup to perform if mode == install
        # First: Let's create a BaseFolder for our app, "Manage"
        # Second: Create the App Component subdirs which hold the apps code
        # -- Think of it Like Clicking the blue "Manage" Button \
             of your app on the Coral Apps Page
    """
    if args.mode == 'install':
        if not os.path.exists(APP_PATH):
            os.makedirs(APP_PATH)
            write_to_file("", "", "__init__.py")

        # Now, let's create separate folders for each of the components
        # under the "Manage/" Dir -- All Component Dirs are made regardless
        # of whether if they are being currently used
        try:
            for comp_type in COMPONENT_TYPES:
                path = os.path.join(APP_PATH, comp_type["type_name"])
                initfile = os.path.join(APP_PATH, comp_type["type_name"], "__init__.py")
                if not os.path.exists(path):
                    os.makedirs(path)
                if not os.path.exists(initfile):
                    open(initfile, 'a').close()
        except:
            pass

        print("%s Path and subdirs have been created" % APP_PATH)


# We first get our app (this is a collection) by name
app = db.app.find_one({"name": APP_NAME})

# The -m mode arg can be get or pull, or put or push, or install which
# performs get/pull with additional setup
if args.mode in ['get', 'pull', 'install']:
    if args.mode == 'install':
        # Run setup first then sync from db
        install()
    else:
        pass
    db_to_files()
elif args.mode in ['put', 'push']:
    files_to_db()
