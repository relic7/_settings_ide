#!/usr/local/bin/bash

#PATTERN="${1:-import}"
DPP="${2:-/data01/commands/ipynb-notebooks/diagnostic-power-packs}"
PATTERN='PolicyName'
STRIP_OUTPUT="[\|]"
FILES_LEGACY=$(ag -r --ini --nofilename -A 2 -H -print0 -s --ignore "/data01/commands/ipynb-notebooks/diagnostic-power-packs/cisco_cms_utils/" \
        --ignore __init__.py --ignore build-content-libraries.py \
        --ignore test_helper.py --ignore test_utils.py \
        --ignore "$DPP/docs/" --ignore "$DPP/fw_docs/" \
        --ignore "$DPP/tools/" --ignore "$DPP/ContentLibraries/" \
        --ignore "$DPP/examples/" --ignore "$DPP/docs/examples/" \
        --ignore "$DPP/cisco_cms_helper/"  \
        --ignore "$DPP/cisco_cms_work_instructions/"  --ignore "$DPP/cisco_cms_jinja2/" \
        --ignore "$DPP/cisco_cms_markupsafe/" \
        --ignore "$DPP/DynamicApplications/" --ignore "$DPP/PowerPacks/" \
        --ignore "$DPP/Poller/" "$PATTERN" "$DPP" | grep -v '\/cisco_cms*\|/docs/\|\/tools\/\|\/examples\/' | xargs -L1
        )

REMOVE='from cisco_cms_helper import Helpers \
        from silo_common.database import local_db \ 
        cms = Helpers(EM7_VALUES, local_db())'

echo "${PATTERN}--${REMOVE}"
#echo "$FILES_LEGACY"
for f in "$FILES_LEGACY";
        #output=`sed "/${REMOVE}/d" "$f"`
        #echo "$output";

do
  echo "${STRIP_OUTPUT} ${f}"
  #f =  $(echo "`pwd`/$f" | sed -e "s/${STRIP_OUTPUT}//g")
  echo -e "$f\n----------------------------------------------------------------------------\n"

done;
        end_of_file=0
#        while [[ $end_of_file == 0 ]]; do
#          #read -r line
#          # the last exit status is the 
#          # flag of the end of file
#          end_of_file=$?
#          #echo "${end_of_file}\n----------------------------------------------------------------------------\n"
#        done #< "$f"
#done;


