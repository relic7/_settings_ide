#!/usr/bin/env python

def dump(obj):
    for attr in dir(obj):
        print('obj.%s = %s' % (attr, getattr(obj, attr)))





if __name__ == '__main__':
    from sys import argv
    dump(argv[1])


