#!/usr/local/bin/python
"""
   Pycharm will need to have a /coral-services/coral directory defined and available within the project to import coral core components if needed by the app.
   The interpreter will need to be set up to use the interpreter that is running the coral instance the app is being built on via a Docker integration or an ssh connection.
   Create files in the App.... directories that match the component names created in your app and copy the code from the coral app interface in the browser to your files on your system, which will be recognized by your IDE.
   This will not bypass or integrate properly with App2Dir functionality for posting code to bitbucket, but will allow for coding in a more familiar environment with constant static analysis/linting of your code and help prevent simple syntax and other preventable issues from escalating, while keeping your code clean.
   Logic and other issues in your code are still up to you to debug, but this allows you to rule out some obvious issues that may have slipped past.

"""
from os import chdir, path, getcwd, makedirs, utime

app_components = { "AppCliCallback": "__init__.py",
                    "AppCollection":  "__init__.py",
                    "AppDiscoveryCallback":  "__init__.py",
                    "AppDeviceCallback":     "__init__.py",
                    "AppDispatcher": "__init__.py",
                    "AppForm":       "__init__.py",
                    "AppModule":     "__init__.py",
                    "AppSchedule":   "__init__py",
                    "AppStandaloneCallback":  "__init__.py",
                    "AppWebhook":   "__init__py",
                    "AppWebpage":   "__init__py",
                    "AppWebview":   "__init__py"
                    }


def generate_appdir_struct(components):
    """
    Generates the App Structure of componenents in the filesystem to work
    with PyCharm and other IDES in a familiar setup to the App interface.
        The directorys will match the component headers
        and the init file will allow importing the componentents into other components
    """
    # Make Top level "Manage" Directory
    basedname = path.join(getcwd(), "Manage")
    if not path.exists(basedname):
        makedirs(basedname)
    chdir(basedname)
    # Add Component Dirs with Manage
    for k,v in components.iteritems():
        dname = path.join(getcwd(), k)
        if not path.exists(dname):
            makedirs(dname)
        fname = path.join(dname, v)
        with open(fname, 'a'):
            utime(fname, None)




if __name__ == '__main__':
    import sys
    #rootdir = sys.argv[1] or "."
    generate_appdir_struct(app_components)
