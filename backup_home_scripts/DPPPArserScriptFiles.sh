#!/bin/bash

PATTERN="${1:-import}"
DPP="${2:-/data01/commands/ipynb-notebooks/diagnostic-power-packs}"

ag -R --python -print0 --stats -s -l --ignore "/data01/commands/ipynb-notebooks/diagnostic-power-packs/cisco_cms_utils/" \
        --ignore __init__.py --ignore build-content-libraries.py \
        --ignore test_helper.py --ignore test_utils.py \
        --ignore "$DPP/docs/" --ignore "$DPP/fw_docs/" \
        --ignore "$DPP/tools/" --ignore "$DPP/ContentLibraries/" \
        --ignore "$DPP/examples/" --ignore "$DPP/docs/examples/" \
        --ignore "$DPP/cisco_cms_helper/"  \
        --ignore "$DPP/cisco_cms_work_instructions/"  --ignore "$DPP/cisco_cms_jinja2/" \
        --ignore "$DPP/cisco_cms_markupsafe/" \
        --ignore "$DPP/DynamicApplications/" --ignore "$DPP/PowerPacks/" \
        --ignore "$DPP/Poller/" "$PATTERN" "$DPP" | grep -v '\/cisco_cms*\|/docs/\|\/tools\/\|\/examples\/' | xargs -L1

export DPP=$DPP

echo "${PATTERN}--${DPP}"


# python -c 'import os; X=list(os.environ.items()); print(str(os.environ["DPP"]) + "______" + str(sorted(str(X).split(","))))'