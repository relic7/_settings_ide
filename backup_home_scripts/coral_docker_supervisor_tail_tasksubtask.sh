#!/bin/bash

# Run as daemon with -d
ANCESTOR=${2:-:latest}
case "$1" in
    -t|--follow-task1|-f|--task)
        
        docker exec -it $(docker ps --filter ancestor=coral-services"${ANCESTOR}" -q | head -1) supervisorctl tail -f task.1 | ccze -A
        exit 0
        ;;
    -s|--follow-subtask1)
        docker exec -it $(docker ps --filter ancestor=coral-services"${ANCESTOR}" -q | head -1) supervisorctl tail -f subtask.1 | ccze -A
        exit 0
        ;;
    -u|--follow-uwsgi)
        docker exec -it $(docker ps --filter ancestor=coral-services"${ANCESTOR}" -q | head -1) supervisorctl tail -f uwsgi | ccze -A
        exit 0
        ;;
    *)
        ;;
esac

# docker exec -it $(docker ps --filter ancestor=coral-services -q | head -1) supervisorctl tail subtask.1


