import logging, time
from os import getcwd

def pylogger():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    filename = time.strftime("my-%Y-%m-%d.log").replace('my',str(getcwd().split('/')[-1]))
    fh = logging.FileHandler(filename)
    fh.setLevel(logging.INFO)
    fh.setFormatter( logging.Formatter('%(asctime)s - %(name)s - %(levelname)-10s - %(message)s') )
    logger.addHandler(fh)
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)
    sh.setFormatter(logging.Formatter('%(asctime)s - %(levelname)-10s - %(message)s'))
    logger.addHandler(sh)
    log = logging.getLogger(__name__)
    log.debug("debug") 
    log.info("info") 
    log.warning("warning") 
    log.error("error")
    log.critical("critical")
    pylogger = log
    return pylogger
