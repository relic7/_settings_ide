#!/usr/local/bin/bash
# ensure running as root
if [ "$(id -u)" != "0" ]; then
  exec sudo "$0" "$@"
fi
source ../.bash_profile
OUT=`ps aux | grep tunnel | awk '{ print $2}' | xargs`


for x in $OUT; do
     echo "Kill Tunnel Job ${x}" 
    kill  "$x"
done;
