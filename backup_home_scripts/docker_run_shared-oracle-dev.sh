#!/bin/bash

export ORACLE_CONTAINER=oracle-dev
export ORACLE_PORT=1521

docker run --rm -d -i -t --privileged \
	--name "$ORACLE_CONTAINER" \
	--hostname "$ORACLE_CONTAINER" \
	-p "$ORACLE_PORT":"$ORACLE_PORT" \
	-v /data01/oracledb/data:/u01/app/oracle \
	relic7/oracle-dev
