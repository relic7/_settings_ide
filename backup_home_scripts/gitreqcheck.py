#!/usr/bin/env python

import argparse
import re


def parse_requirements(requirements):
    """
    Parse pip requirements definition file and return list of tuples

    Args:
        requirements: Pip requirements definition as a text file

    Returns:
        List of tuples. Each tuple contains package name and version

    """
    result = []
    with open(requirements, 'r') as fh:
        for line in fh.readlines():
            m = re.match('^(?P<package>[^#].+?)(?:\=+(?P<version>.+))?$', line, flags=re.IGNORECASE)

            if m:
                if m.group('package'):
                    if m.group('version'):
                        result.append((m.group('package').lower(), m.group('version').lower()))
                    else:
                        result.append((m.group('package').lower(),))
        fh.close()
    return result




def parse_piplist(piplist):
    """
    Parse piplist output into a dictionary

    Args:
        piplist: Pip list output as a text file

    Returns:
        Dictionary containing package name as a key and version as a value
    """
    result = {}
    with open(piplist, 'r') as fh:
        for line in fh.readlines():
            m = re.match('^(?P<package>[^#].+?)[\s]+(?P<version>.+)$', line, flags=re.IGNORECASE)

            if m:
                if m.group('package') and m.group('version'):
                    result[m.group('package').lower()] = m.group('version').lower()
        fh.close()
    return result


def check(require_list, piplist_dict):
    for r in require_list:
        if len(r) > 1:
            # Has version in requirements so they must match
            if piplist_dict.get(r[0]) != r[1]:
                print('{p!s} {rv!s} != {plv!s}'.format(p=r[0],
                                                       rv=r[1],
                                                       plv=piplist_dict.get(r[0])))
        else:
            # No version in requirements so package must simply exist
            if r[0] not in piplist_dict.keys():
                print('{p!s} not found'.format(p=r[0]))


if __name__ == '__main__':
    """
    This program will compare a pip requirements document to pip list output and print conflicts that it finds
    """
    parser = argparse.ArgumentParser(description="This program will compare a pip requirements document to pip " + \
            "list output and print conflicts that it finds")
    parser.add_argument('requirements',
                        help='Pip requirements definition as a text file')
    parser.add_argument('piplist',
                        help='Pip list output as a text file to check against requirements')
    argp = parser.parse_args()

    r = parse_requirements(argp.requirements)
    l = parse_piplist(argp.piplist)
    check(require_list=r, piplist_dict=l)
